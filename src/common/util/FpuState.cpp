#include "FpuState.h"
#include "../simde/simde/x86/sse2.h"
#include <float.h>

FpuState::FpuState() : _old_SSE_state(0), _SSE_Flags(0x8040)
{}

void FpuState::set()
{
   bool fpuExceptions = false;

   _old_SSE_state = simde_mm_getcsr();
   if (fpuExceptions)
   {
      simde_mm_setcsr(((_old_SSE_state & ~SIMDE_MM_MASK_MASK) | _SSE_Flags) | SIMDE_MM_EXCEPT_MASK); // all on
   }
   else
   {
      simde_mm_setcsr((_old_SSE_state | _SSE_Flags) | SIMDE_MM_MASK_MASK);
   }
   // FTZ/DAZ + ignore all exceptions (1 means ignored)

   SIMDE_MM_SET_ROUNDING_MODE(SIMDE_MM_ROUND_NEAREST);
}

void FpuState::restore()
{
   simde_mm_setcsr(_old_SSE_state);
}
