#include "lipol.h"

const simde__m128 two = simde_mm_set1_ps(2.f);
const simde__m128 four = simde_mm_set1_ps(4.f);

lipol_ps::lipol_ps()
{
   target = simde_mm_setzero_ps();
   currentval = simde_mm_setzero_ps();
   coef = simde_mm_set1_ps(0.25f);
   coef_m1 = simde_mm_sub_ss(m128_one, coef);
   m128_lipolstarter = simde_mm_set_ps(1.f, 0.75f, 0.5f, 0.25f);
   set_blocksize(64);
}

void lipol_ps::set_blocksize(int bs)
{
   lipol_BLOCK_SIZE = simde_mm_cvt_si2ss(m128_zero, bs);
   m128_bs4_inv = simde_mm_div_ss(m128_four, lipol_BLOCK_SIZE);
}

void lipol_ps::multiply_block(float* src, unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   unsigned int n = nquads << 2;
   for (unsigned int i = 0; (i < n); i += 8) // nquads must be multiple of 4
   {
      simde__m128 a = simde_mm_mul_ps(simde_mm_load_ps(src + i), y1);
      simde_mm_store_ps(src + i, a);
      y1 = simde_mm_add_ps(y1, dy);
      simde__m128 b = simde_mm_mul_ps(simde_mm_load_ps(src + i + 4), y2);
      simde_mm_store_ps(src + i + 4, b);
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::multiply_block_sat1(float* src, unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   const simde__m128 satv = simde_mm_set1_ps(1.0f);

   for (unsigned int i = 0; (i < nquads << 2); i += 8) // nquads must be multiple of 4
   {
      simde_mm_store_ps(src + i, simde_mm_mul_ps(simde_mm_load_ps(src + i), y1));
      y1 = simde_mm_min_ps(satv, simde_mm_add_ps(y1, dy));
      simde_mm_store_ps(src + i + 4, simde_mm_mul_ps(simde_mm_load_ps(src + i + 4), y2));
      y2 = simde_mm_min_ps(satv, simde_mm_add_ps(y2, dy));
   }
}

void lipol_ps::store_block(float* dst, unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads << 2; i += 8) // nquads must be multiple of 4
   {
      simde_mm_store_ps(dst + i, y1);
      y1 = simde_mm_add_ps(y1, dy);
      simde_mm_store_ps(dst + i + 4, y2);
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::add_block(float* src, unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads; i += 2) // nquads must be multiple of 4
   {
      ((simde__m128*)src)[i] = simde_mm_add_ps(((simde__m128*)src)[i], y1);
      y1 = simde_mm_add_ps(y1, dy);
      ((simde__m128*)src)[i + 1] = simde_mm_add_ps(((simde__m128*)src)[i + 1], y2);
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::subtract_block(float* src, unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads; i += 2) // nquads must be multiple of 4
   {
      ((simde__m128*)src)[i] = simde_mm_sub_ps(((simde__m128*)src)[i], y1);
      y1 = simde_mm_add_ps(y1, dy);
      ((simde__m128*)src)[i + 1] = simde_mm_sub_ps(((simde__m128*)src)[i + 1], y2);
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::multiply_2_blocks(float* __restrict src1,
                                 float* __restrict src2,
                                 unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads; i += 2) // nquads must be multiple of 4
   {
      ((simde__m128*)src1)[i] = simde_mm_mul_ps(((simde__m128*)src1)[i], y1);
      ((simde__m128*)src2)[i] = simde_mm_mul_ps(((simde__m128*)src2)[i], y1);
      y1 = simde_mm_add_ps(y1, dy);
      ((simde__m128*)src1)[i + 1] = simde_mm_mul_ps(((simde__m128*)src1)[i + 1], y2);
      ((simde__m128*)src2)[i + 1] = simde_mm_mul_ps(((simde__m128*)src2)[i + 1], y2);
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::MAC_block_to(float* __restrict src, float* __restrict dst, unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads; i += 2) // nquads must be multiple of 4
   {
      ((simde__m128*)dst)[i] = simde_mm_add_ps(((simde__m128*)dst)[i], simde_mm_mul_ps(((simde__m128*)src)[i], y1));
      y1 = simde_mm_add_ps(y1, dy);
      ((simde__m128*)dst)[i + 1] =
          simde_mm_add_ps(((simde__m128*)dst)[i + 1], simde_mm_mul_ps(((simde__m128*)src)[i + 1], y2));
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::MAC_2_blocks_to(float* __restrict src1,
                               float* __restrict src2,
                               float* __restrict dst1,
                               float* __restrict dst2,
                               unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads; i += 2) // nquads must be multiple of 4
   {
      ((simde__m128*)dst1)[i] = simde_mm_add_ps(((simde__m128*)dst1)[i], simde_mm_mul_ps(((simde__m128*)src1)[i], y1));
      ((simde__m128*)dst2)[i] = simde_mm_add_ps(((simde__m128*)dst2)[i], simde_mm_mul_ps(((simde__m128*)src2)[i], y1));
      y1 = simde_mm_add_ps(y1, dy);
      ((simde__m128*)dst1)[i + 1] =
          simde_mm_add_ps(((simde__m128*)dst1)[i + 1], simde_mm_mul_ps(((simde__m128*)src1)[i + 1], y2));
      ((simde__m128*)dst2)[i + 1] =
          simde_mm_add_ps(((simde__m128*)dst2)[i + 1], simde_mm_mul_ps(((simde__m128*)src2)[i + 1], y2));
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::multiply_block_to(float* __restrict src, float* __restrict dst, unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads; i += 2) // nquads must be multiple of 4
   {
      simde__m128 a = simde_mm_mul_ps(((simde__m128*)src)[i], y1);
      ((simde__m128*)dst)[i] = a;
      y1 = simde_mm_add_ps(y1, dy);

      simde__m128 b = simde_mm_mul_ps(((simde__m128*)src)[i + 1], y2);
      ((simde__m128*)dst)[i + 1] = b;
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::multiply_2_blocks_to(float* __restrict src1,
                                    float* __restrict src2,
                                    float* __restrict dst1,
                                    float* __restrict dst2,
                                    unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads; i += 2) // nquads must be multiple of 4
   {
      ((simde__m128*)dst1)[i] = simde_mm_mul_ps(((simde__m128*)src1)[i], y1);
      ((simde__m128*)dst2)[i] = simde_mm_mul_ps(((simde__m128*)src2)[i], y1);
      y1 = simde_mm_add_ps(y1, dy);
      ((simde__m128*)dst1)[i + 1] = simde_mm_mul_ps(((simde__m128*)src1)[i + 1], y2);
      ((simde__m128*)dst2)[i + 1] = simde_mm_mul_ps(((simde__m128*)src2)[i + 1], y2);
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::trixpan_blocks(float* __restrict L,
                              float* __restrict R,
                              float* __restrict dL,
                              float* __restrict dR,
                              unsigned int nquads)
{
   simde__m128 y, dy;
   initblock(y, dy);

   for (unsigned int i = 0; i < nquads; i++)
   {
      simde__m128 a = simde_mm_max_ps(m128_zero, y);
      simde__m128 b = simde_mm_min_ps(m128_zero, y);
      simde__m128 tL = simde_mm_sub_ps(simde_mm_mul_ps(simde_mm_sub_ps(m128_one, a), ((simde__m128*)L)[i]),
                             simde_mm_mul_ps(b, ((simde__m128*)R)[i])); // L = (1-a)*L - b*R
      simde__m128 tR =
          simde_mm_add_ps(simde_mm_mul_ps(a, ((simde__m128*)L)[i]),
                     simde_mm_mul_ps(simde_mm_add_ps(m128_one, b), ((simde__m128*)R)[i])); // R = a*L + (1+b)*R
      ((simde__m128*)dL)[i] = tL;
      ((simde__m128*)dR)[i] = tR;
      y = simde_mm_add_ps(y, dy);
   }
}

void lipol_ps::fade_block_to(float* __restrict src1,
                             float* __restrict src2,
                             float* __restrict dst,
                             unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads; i += 2) // nquads must be multiple of 4
   {
      simde__m128 a = simde_mm_mul_ps(((simde__m128*)src1)[i], simde_mm_sub_ps(m128_one, y1));
      simde__m128 b = simde_mm_mul_ps(((simde__m128*)src2)[i], y1);
      ((simde__m128*)dst)[i] = simde_mm_add_ps(a, b);
      y1 = simde_mm_add_ps(y1, dy);

      a = simde_mm_mul_ps(((simde__m128*)src1)[i + 1], simde_mm_sub_ps(m128_one, y2));
      b = simde_mm_mul_ps(((simde__m128*)src2)[i + 1], y2);
      ((simde__m128*)dst)[i + 1] = simde_mm_add_ps(a, b);
      y2 = simde_mm_add_ps(y2, dy);
   }
}

void lipol_ps::fade_2_blocks_to(float* __restrict src11,
                                float* __restrict src12,
                                float* __restrict src21,
                                float* __restrict src22,
                                float* __restrict dst1,
                                float* __restrict dst2,
                                unsigned int nquads)
{
   simde__m128 y1, y2, dy;
   initblock(y1, dy);
   y2 = simde_mm_add_ps(y1, dy);
   dy = simde_mm_mul_ps(dy, two);

   for (unsigned int i = 0; i < nquads; i += 2) // nquads must be multiple of 4
   {
      simde__m128 a = simde_mm_mul_ps(((simde__m128*)src11)[i], simde_mm_sub_ps(m128_one, y1));
      simde__m128 b = simde_mm_mul_ps(((simde__m128*)src12)[i], y1);
      ((simde__m128*)dst1)[i] = simde_mm_add_ps(a, b);
      a = simde_mm_mul_ps(((simde__m128*)src21)[i], simde_mm_sub_ps(m128_one, y1));
      b = simde_mm_mul_ps(((simde__m128*)src22)[i], y1);
      ((simde__m128*)dst2)[i] = simde_mm_add_ps(a, b);
      y1 = simde_mm_add_ps(y1, dy);

      a = simde_mm_mul_ps(((simde__m128*)src11)[i + 1], simde_mm_sub_ps(m128_one, y2));
      b = simde_mm_mul_ps(((simde__m128*)src12)[i + 1], y2);
      ((simde__m128*)dst1)[i + 1] = simde_mm_add_ps(a, b);
      a = simde_mm_mul_ps(((simde__m128*)src21)[i + 1], simde_mm_sub_ps(m128_one, y2));
      b = simde_mm_mul_ps(((simde__m128*)src22)[i + 1], y2);
      ((simde__m128*)dst2)[i + 1] = simde_mm_add_ps(a, b);
      y2 = simde_mm_add_ps(y2, dy);
   }
}
