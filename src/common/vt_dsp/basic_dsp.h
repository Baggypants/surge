#pragma once
#include "shared.h"

int Min(int a, int b);
int Max(int a, int b);
double Max(double a, double b);
unsigned int Min(unsigned int a, unsigned int b);
unsigned int Max(unsigned int a, unsigned int b);
int limit_range(int x, int low, int high);
unsigned int limit_range(unsigned int x, unsigned int low, unsigned int high);
double limit_range(double x, double low, double high);
int Float2Int(float x);
unsigned int Float2UInt(float x);
int Wrap(int x, int low, int high);
int Sign(int x);

void hardclip_block(float* x, unsigned int nquads);
void hardclip_block8(float* x, unsigned int nquads);
void softclip_block(float* in, unsigned int nquads);
void tanh7_block(float* x, unsigned int nquads);
void clear_block(float* in, unsigned int nquads);
void clear_block_antidenormalnoise(float* in, unsigned int nquads);
void accumulate_block(float* src, float* dst, unsigned int nquads);
void copy_block(float* src, float* dst, unsigned int nquads); // copy block (requires aligned data)
void copy_block_US(float* src, float* dst, unsigned int nquads); // copy block (unaligned source)
void copy_block_UD(float* src,
                   float* dst,
                   unsigned int nquads); // copy block (unaligned destination)
void copy_block_USUD(float* src,
                     float* dst,
                     unsigned int nquads); // copy block (unaligned source + destination)
void mul_block(float* src1, float* src2, float* dst, unsigned int nquads);
void add_block(float* src1, float* src2, float* dst, unsigned int nquads);
void subtract_block(float* src1, float* src2, float* dst, unsigned int nquads);
void encodeMS(float* L, float* R, float* M, float* S, unsigned int nquads);
void decodeMS(float* M, float* S, float* L, float* R, unsigned int nquads);
float get_absmax(float* d, unsigned int nquads);
float get_squaremax(float* d, unsigned int nquads);
float get_absmax_2(float* d1, float* d2, unsigned int nquads);
void float2i15_block(float*, short*, int);
void i152float_block(short*, float*, int);

float sine_ss(unsigned int x);
int sine(int x);

inline float limit_range(float x, float low, float high)
{
   float result;
   simde_mm_store_ss(&result,
                simde_mm_min_ss(simde_mm_max_ss(simde_mm_load_ss(&x), simde_mm_load_ss(&low)), simde_mm_load_ss(&high)));
   return result;
}

inline simde__m128 sum_ps_to_ss(simde__m128 x)
{
   /*simde__m128 a = simde_mm_add_ss(x,simde_mm_shuffle_ps(x,x,SIMDE_MM_SHUFFLE(0,0,0,1)));
   simde__m128 b =
   simde_mm_add_ss(simde_mm_shuffle_ps(x,x,SIMDE_MM_SHUFFLE(0,0,0,2)),simde_mm_shuffle_ps(x,x,SIMDE_MM_SHUFFLE(0,0,0,3)));
   return simde_mm_add_ss(a,b);*/
   simde__m128 a = simde_mm_add_ps(x, simde_mm_movehl_ps(x, x));
   return simde_mm_add_ss(a, simde_mm_shuffle_ps(a, a, SIMDE_MM_SHUFFLE(0, 0, 0, 1)));
}

inline simde__m128 max_ps_to_ss(simde__m128 x)
{
   simde__m128 a = simde_mm_max_ss(x, simde_mm_shuffle_ps(x, x, SIMDE_MM_SHUFFLE(0, 0, 0, 1)));
   simde__m128 b = simde_mm_max_ss(simde_mm_shuffle_ps(x, x, SIMDE_MM_SHUFFLE(0, 0, 0, 2)),
                         simde_mm_shuffle_ps(x, x, SIMDE_MM_SHUFFLE(0, 0, 0, 3)));
   return simde_mm_max_ss(a, b);
}

inline simde__m128 hardclip_ss(simde__m128 x)
{
   const simde__m128 x_min = simde_mm_set_ss(-1.0f);
   const simde__m128 x_max = simde_mm_set_ss(1.0f);
   return simde_mm_max_ss(simde_mm_min_ss(x, x_max), x_min);
}

inline float rcp(float x)
{
   simde_mm_store_ss(&x, simde_mm_rcp_ss(simde_mm_load_ss(&x)));
   return x;
}

inline simde__m128d hardclip8_sd(simde__m128d x)
{
   const simde__m128d x_min = simde_mm_set_sd(-7.0f);
   const simde__m128d x_max = simde_mm_set_sd(8.0f);
   return simde_mm_max_sd(simde_mm_min_sd(x, x_max), x_min);
}

inline simde__m128 hardclip_ps(simde__m128 x)
{
   const simde__m128 x_min = simde_mm_set1_ps(-1.0f);
   const simde__m128 x_max = simde_mm_set1_ps(1.0f);
   return simde_mm_max_ps(simde_mm_min_ps(x, x_max), x_min);
}

inline float saturate(float f)
{
   simde__m128 x = simde_mm_load_ss(&f);
   const simde__m128 x_min = simde_mm_set_ss(-1.0f);
   const simde__m128 x_max = simde_mm_set_ss(1.0f);
   x = simde_mm_max_ss(simde_mm_min_ss(x, x_max), x_min);
   simde_mm_store_ss(&f, x);
   return f;
}

inline simde__m128 softclip_ss(simde__m128 in)
{
   // y = x - (4/27)*x^3,  x € [-1.5 .. 1.5]
   const simde__m128 a = simde_mm_set_ss(-4.f / 27.f);

   const simde__m128 x_min = simde_mm_set_ss(-1.5f);
   const simde__m128 x_max = simde_mm_set_ss(1.5f);

   simde__m128 x = simde_mm_max_ss(simde_mm_min_ss(in, x_max), x_min);
   simde__m128 xx = simde_mm_mul_ss(x, x);
   simde__m128 t = simde_mm_mul_ss(x, a);
   t = simde_mm_mul_ss(t, xx);
   t = simde_mm_add_ss(t, x);

   return t;
}

inline simde__m128 softclip_ps(simde__m128 in)
{
   // y = x - (4/27)*x^3,  x € [-1.5 .. 1.5]
   const simde__m128 a = simde_mm_set1_ps(-4.f / 27.f);

   const simde__m128 x_min = simde_mm_set1_ps(-1.5f);
   const simde__m128 x_max = simde_mm_set1_ps(1.5f);

   simde__m128 x = simde_mm_max_ps(simde_mm_min_ps(in, x_max), x_min);
   simde__m128 xx = simde_mm_mul_ps(x, x);
   simde__m128 t = simde_mm_mul_ps(x, a);
   t = simde_mm_mul_ps(t, xx);
   t = simde_mm_add_ps(t, x);

   return t;
}

inline simde__m128 tanh7_ps(simde__m128 x)
{
   const simde__m128 a = simde_mm_set1_ps(-1.f / 3.f);
   const simde__m128 b = simde_mm_set1_ps(2.f / 15.f);
   const simde__m128 c = simde_mm_set1_ps(-17.f / 315.f);
   const simde__m128 one = simde_mm_set1_ps(1.f);
   simde__m128 xx = simde_mm_mul_ps(x, x);
   simde__m128 y = simde_mm_add_ps(one, simde_mm_mul_ps(a, xx));
   simde__m128 x4 = simde_mm_mul_ps(xx, xx);
   y = simde_mm_add_ps(y, simde_mm_mul_ps(b, x4));
   x4 = simde_mm_mul_ps(x4, xx);
   y = simde_mm_add_ps(y, simde_mm_mul_ps(c, x4));
   return simde_mm_mul_ps(y, x);
}

inline simde__m128 tanh7_ss(simde__m128 x)
{
   const simde__m128 a = simde_mm_set1_ps(-1.f / 3.f);
   const simde__m128 b = simde_mm_set1_ps(2.f / 15.f);
   const simde__m128 c = simde_mm_set1_ps(-17.f / 315.f);
   const simde__m128 one = simde_mm_set1_ps(1.f);
   simde__m128 xx = simde_mm_mul_ss(x, x);
   simde__m128 y = simde_mm_add_ss(one, simde_mm_mul_ss(a, xx));
   simde__m128 x4 = simde_mm_mul_ss(xx, xx);
   y = simde_mm_add_ss(y, simde_mm_mul_ss(b, x4));
   x4 = simde_mm_mul_ss(x4, xx);
   y = simde_mm_add_ss(y, simde_mm_mul_ss(c, x4));
   return simde_mm_mul_ss(y, x);
}

inline simde__m128 abs_ps(simde__m128 x)
{
   return simde_mm_and_ps(x, m128_mask_absval);
}

inline simde__m128 softclip8_ps(simde__m128 in)
{
   const simde__m128 a = simde_mm_set1_ps(-0.00028935185185f);

   const simde__m128 x_min = simde_mm_set1_ps(-12.f);
   const simde__m128 x_max = simde_mm_set1_ps(12.f);

   simde__m128 x = simde_mm_max_ps(simde_mm_min_ps(in, x_max), x_min);
   simde__m128 xx = simde_mm_mul_ps(x, x);
   simde__m128 t = simde_mm_mul_ps(x, a);
   t = simde_mm_mul_ps(t, xx);
   t = simde_mm_add_ps(t, x);
   return t;
}

inline double tanh7_double(double x)
{
   const double a = -1 / 3, b = 2 / 15, c = -17 / 315;
   // return tanh(x);
   double xs = x * x;
   double y = 1 + xs * a + xs * xs * b + xs * xs * xs * c;
   // double y = 1 + xs*(a + xs*(b + xs*c));
   // t = xs*c;
   // t += b
   // t *= xs;
   // t += a;
   // t *= xs;
   // t += 1;
   // t *= x;

   return y * x;
}

inline double softclip_double(double in)
{
   const double c0 = (4.0 / 27.0);
   double x = (in > -1.5f) ? in : -1.5;
   x = (x < 1.5f) ? x : 1.5;
   double ax = c0 * x;
   double xx = x * x;
   return x - ax * xx;
}

inline double softclip4_double(double in)
{
   double x = (in > -6.0) ? in : -6.0;
   x = (x < 6.0) ? x : 6.0;
   double ax = 0.0023148148148 * x; // 1.0 / 27*16
   double xx = x * x;
   return x - ax * xx;
}

inline double softclip8_double(double in)
{
   double x = (in > -12.0) ? in : -12.0;
   x = (x < 12.0) ? x : 12.0;
   double ax = 0.00028935185185 * x; // 1.0 / 27*128
   double xx = x * x;
   return x - ax * xx;
}

inline double softclip2_double(double in)
{
   double x = (in > -3.0) ? in : -3.0;
   x = (x < 3.0) ? x : 3.0;
   double ax = 0.185185185185185185 * x; // 1.0 / 27*2
   double xx = x * x;
   return x - ax * xx;
}

inline float megapanL(float pos) // valid range -2 .. 2 (> +- 1 is inverted phase)
{
   if (pos > 2.f)
      pos = 2.f;
   if (pos < -2.f)
      pos = -2.f;
   return (1 - 0.75f * pos - 0.25f * pos * pos);
}

inline float megapanR(float pos)
{
   if (pos > 2.f)
      pos = 2.f;
   if (pos < -2.f)
      pos = -2.f;
   return (1 + 0.75f * pos - 0.25f * pos * pos);
}
