#if LINUX
#include "../../simde/simde/x86/sse2.h"
#endif

#define vFloat simde__m128

#define vZero simde_mm_setzero_ps()

#define vAdd simde_mm_add_ps
#define vSub simde_mm_sub_ps
#define vMul simde_mm_mul_ps
#define vMAdd(a, b, c) simde_mm_add_ps(simde_mm_mul_ps(a, b), c)
#define vNMSub(a, b, c) simde_mm_sub_ps(c, simde_mm_mul_ps(a, b))

#define vAnd simde_mm_and_ps
#define vOr simde_mm_or_ps

#define vCmpGE simde_mm_cmpge_ps

#define vMax simde_mm_max_ps
#define vMin simde_mm_min_ps

#define vLoad simde_mm_load_ps

inline vFloat vLoad1(float f)
{
   return simde_mm_load1_ps(&f);
}

inline vFloat vSqrtFast(vFloat v)
{
   return simde_mm_rcp_ps(simde_mm_rsqrt_ps(v));
}

inline float vSum(vFloat x)
{
   simde__m128 a = simde_mm_add_ps(x, simde_mm_movehl_ps(x, x));
   a = simde_mm_add_ss(a, simde_mm_shuffle_ps(a, a, SIMDE_MM_SHUFFLE(0, 0, 0, 1)));
   float f;
   simde_mm_store_ss(&f, a);

   return f;
}
