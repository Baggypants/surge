#include "basic_dsp.h"
#include <assert.h>
#include <math.h>
#if MAC || LINUX
#include <algorithm>
#endif

using namespace std;

int Min(int a, int b)
{
#if _M_X64 || LINUX || TARGET_RACK
   return min(a, b);
#else
   __asm
   {
		mov		eax, a
		mov		ecx, b
		cmp		eax, ecx
		cmovg	eax, ecx
   }
#endif
}
int Max(int a, int b)
{
#if _M_X64 || LINUX || TARGET_RACK
   return max(a, b);
#else
   __asm
   {
		mov		eax, a
		mov		ecx, b
		cmp		eax, ecx
		cmovl	eax, ecx
   }
#endif
}

double Max(double a, double b)
{
#if (_M_IX86_FP > 1)
   simde_mm_store_sd(&a, simde_mm_max_sd(simde_mm_load_sd(&a), simde_mm_load_sd(&b)));
   return a;
#else
   if (a > b)
      return a;
   return b;
#endif
}

unsigned int Min(unsigned int a, unsigned int b)
{
#if _M_X64 || LINUX || TARGET_RACK
   return min(a, b);
#else
   __asm
   {
		mov		eax, a
		mov		ecx, b
		cmp		eax, ecx
		cmova	eax, ecx
   }
#endif
}
unsigned int Max(unsigned int a, unsigned int b)
{
#if _M_X64 || LINUX || TARGET_RACK
   return max(a, b);
#else
   __asm
   {
		mov		eax, a
		mov		ecx, b
		cmp		eax, ecx
		cmovb	eax, ecx
   }
#endif
}

int limit_range(int x, int l, int h)
{
#if _M_X64 || LINUX || MAC
   return std::max(std::min(x, h), l);
#else
   __asm
   {
		mov		eax, x
		mov		edx, l
		cmp		eax, edx
		cmovl	eax, edx
		mov		edx, h
		cmp		eax, edx
		cmovg	eax, edx
   }
#endif
}

int Wrap(int x, int L, int H)
{
#if _M_X64 || LINUX || TARGET_RACK
   // don't remember what this was anymore...
   // int diff = H - L;
   // if(x > H) x = x-H;
   assert(0);
   return 0;
#else
   __asm
   {
		; load values
		mov		ecx, H
		mov		edx, ecx		
		mov		eax, x
		; calc diff		
		sub		edx, L		
		; prepare wrapped val 1	into ebx
		mov		ebx, eax
		sub		ebx, H	
		; compare with H
		cmp		eax, ecx
		cmovg	eax, ebx				
		; prepare wrapped val 2 into edx	
		add		edx, eax	
		; compare with L
		cmp		eax, L
		cmovl	eax, edx
   }
#endif
}

int Sign(int x)
{
#if _M_X64 || LINUX || TARGET_RACK
   return (x < 0) ? -1 : 1;
#else
   __asm
       {		
		mov eax, 1
		mov edx, -1		
		cmp x, 0
		cmovle eax, edx
       }
   ;
#endif
}

unsigned int limit_range(unsigned int x, unsigned int l, unsigned int h)
{
#if _M_X64 || LINUX || TARGET_RACK
   return max(min(x, h), l);
#else
   __asm
   {
		mov		eax, x		
		mov		ecx, l	; load min
		cmp		eax, ecx
		cmovb	eax, ecx	; move if below
		mov		ecx, h	; load max
		cmp		eax, ecx
		cmova	eax, ecx	; move if above
   }
#endif
}
/*
float limit_range(float x, float min, float max)
{
   float result;
        simde_mm_store_ss(&result,
simde_mm_min_ss(simde_mm_max_ss(simde_mm_load_ss(&x),simde_mm_load_ss(&min)),simde_mm_load_ss(&max))); return result;
}*/

double limit_range(double x, double min, double max)
{
   if (x > max)
      return max;
   if (x < min)
      return min;
   return x;
}

int Float2Int(float x)
{
   return simde_mm_cvt_ss2si(simde_mm_load_ss(&x));
}

void float2i15_block(float* f, short* s, int n)
{
   for (int i = 0; i < n; i++)
   {
      s[i] = (short)(int)limit_range((int)((float)f[i] * 16384.f), -16384, 16383);
   }
}

void i152float_block(short* s, float* f, int n)
{
   const float scale = 1.f / 16384.f;
   for (int i = 0; i < n; i++)
   {
      f[i] = (float)s[i] * scale;
   }
}

void hardclip_block(float* x, unsigned int nquads)
{
   const simde__m128 x_min = simde_mm_set1_ps(-1.0f);
   const simde__m128 x_max = simde_mm_set1_ps(1.0f);
   for (unsigned int i = 0; i < (nquads << 2); i += 8)
   {
      simde_mm_store_ps(x + i, simde_mm_max_ps(simde_mm_min_ps(simde_mm_load_ps(x + i), x_max), x_min));
      simde_mm_store_ps(x + i + 4, simde_mm_max_ps(simde_mm_min_ps(simde_mm_load_ps(x + i + 4), x_max), x_min));
   }
}

void hardclip_block8(float* x, unsigned int nquads)
{
   const simde__m128 x_min = simde_mm_set1_ps(-8.0f);
   const simde__m128 x_max = simde_mm_set1_ps(8.0f);
   for (unsigned int i = 0; i < (nquads << 2); i += 8)
   {
      simde_mm_store_ps(x + i, simde_mm_max_ps(simde_mm_min_ps(simde_mm_load_ps(x + i), x_max), x_min));
      simde_mm_store_ps(x + i + 4, simde_mm_max_ps(simde_mm_min_ps(simde_mm_load_ps(x + i + 4), x_max), x_min));
   }
}

void softclip_block(float* in, unsigned int nquads)
{
   // y = x - (4/27)*x^3,  x [-1.5 .. 1.5]
   const simde__m128 a = simde_mm_set1_ps(-4.f / 27.f);

   const simde__m128 x_min = simde_mm_set1_ps(-1.5f);
   const simde__m128 x_max = simde_mm_set1_ps(1.5f);

   for (unsigned int i = 0; i < nquads; i += 2)
   {
      simde__m128 x[2], xx[2], t[2];

      x[0] = simde_mm_min_ps(simde_mm_load_ps(in + (i << 2)), x_max);
      x[1] = simde_mm_min_ps(simde_mm_load_ps(in + ((i + 1) << 2)), x_max);

      x[0] = simde_mm_max_ps(x[0], x_min);
      x[1] = simde_mm_max_ps(x[1], x_min);

      xx[0] = simde_mm_mul_ps(x[0], x[0]);
      xx[1] = simde_mm_mul_ps(x[1], x[1]);

      t[0] = simde_mm_mul_ps(x[0], a);
      t[1] = simde_mm_mul_ps(x[1], a);

      t[0] = simde_mm_mul_ps(t[0], xx[0]);
      t[1] = simde_mm_mul_ps(t[1], xx[1]);

      t[0] = simde_mm_add_ps(t[0], x[0]);
      t[1] = simde_mm_add_ps(t[1], x[1]);

      simde_mm_store_ps(in + (i << 2), t[0]);
      simde_mm_store_ps(in + ((i + 1) << 2), t[1]);
   }
}

float get_squaremax(float* d, unsigned int nquads)
{
   simde__m128 mx1 = simde_mm_setzero_ps();
   simde__m128 mx2 = simde_mm_setzero_ps();

   for (unsigned int i = 0; i < nquads; i += 2)
   {
      mx1 = simde_mm_max_ps(mx1, simde_mm_mul_ps(((simde__m128*)d)[i], ((simde__m128*)d)[i]));
      mx2 = simde_mm_max_ps(mx2, simde_mm_mul_ps(((simde__m128*)d)[i + 1], ((simde__m128*)d)[i + 1]));
   }
   mx1 = simde_mm_max_ps(mx1, mx2);
   mx1 = max_ps_to_ss(mx1);
   float f;
   simde_mm_store_ss(&f, mx1);
   return f;
}

float get_absmax(float* d, unsigned int nquads)
{
   simde__m128 mx1 = simde_mm_setzero_ps();
   simde__m128 mx2 = simde_mm_setzero_ps();

   for (unsigned int i = 0; i < nquads; i += 2)
   {
      mx1 = simde_mm_max_ps(mx1, simde_mm_and_ps(((simde__m128*)d)[i], m128_mask_absval));
      mx2 = simde_mm_max_ps(mx2, simde_mm_and_ps(((simde__m128*)d)[i + 1], m128_mask_absval));
   }
   mx1 = simde_mm_max_ps(mx1, mx2);
   mx1 = max_ps_to_ss(mx1);
   float f;
   simde_mm_store_ss(&f, mx1);
   return f;
}

float get_absmax_2(float* __restrict d1, float* __restrict d2, unsigned int nquads)
{
   simde__m128 mx1 = simde_mm_setzero_ps();
   simde__m128 mx2 = simde_mm_setzero_ps();
   simde__m128 mx3 = simde_mm_setzero_ps();
   simde__m128 mx4 = simde_mm_setzero_ps();

   for (unsigned int i = 0; i < nquads; i += 2)
   {
      mx1 = simde_mm_max_ps(mx1, simde_mm_and_ps(((simde__m128*)d1)[i], m128_mask_absval));
      mx2 = simde_mm_max_ps(mx2, simde_mm_and_ps(((simde__m128*)d1)[i + 1], m128_mask_absval));
      mx3 = simde_mm_max_ps(mx3, simde_mm_and_ps(((simde__m128*)d2)[i], m128_mask_absval));
      mx4 = simde_mm_max_ps(mx4, simde_mm_and_ps(((simde__m128*)d2)[i + 1], m128_mask_absval));
   }
   mx1 = simde_mm_max_ps(mx1, mx2);
   mx3 = simde_mm_max_ps(mx3, mx4);
   mx1 = simde_mm_max_ps(mx1, mx3);
   mx1 = max_ps_to_ss(mx1);
   float f;
   simde_mm_store_ss(&f, mx1);
   return f;
}

void tanh7_block(float* xb, unsigned int nquads)
{
   const simde__m128 a = simde_mm_set1_ps(-1.f / 3.f);
   const simde__m128 b = simde_mm_set1_ps(2.f / 15.f);
   const simde__m128 c = simde_mm_set1_ps(-17.f / 315.f);
   const simde__m128 one = simde_mm_set1_ps(1.f);
   const simde__m128 upper_bound = simde_mm_set1_ps(1.139f);
   const simde__m128 lower_bound = simde_mm_set1_ps(-1.139f);

   simde__m128 t[4], x[4], xx[4];

   for (unsigned int i = 0; i < nquads; i += 4)
   {
      x[0] = ((simde__m128*)xb)[i];
      x[1] = ((simde__m128*)xb)[i + 1];
      x[2] = ((simde__m128*)xb)[i + 2];
      x[3] = ((simde__m128*)xb)[i + 3];

      x[0] = simde_mm_max_ps(x[0], lower_bound);
      x[1] = simde_mm_max_ps(x[1], lower_bound);
      x[2] = simde_mm_max_ps(x[2], lower_bound);
      x[3] = simde_mm_max_ps(x[3], lower_bound);
      x[0] = simde_mm_min_ps(x[0], upper_bound);
      x[1] = simde_mm_min_ps(x[1], upper_bound);
      x[2] = simde_mm_min_ps(x[2], upper_bound);
      x[3] = simde_mm_min_ps(x[3], upper_bound);

      xx[0] = simde_mm_mul_ps(x[0], x[0]);
      xx[1] = simde_mm_mul_ps(x[1], x[1]);
      xx[2] = simde_mm_mul_ps(x[2], x[2]);
      xx[3] = simde_mm_mul_ps(x[3], x[3]);

      t[0] = simde_mm_mul_ps(xx[0], c);
      t[1] = simde_mm_mul_ps(xx[1], c);
      t[2] = simde_mm_mul_ps(xx[2], c);
      t[3] = simde_mm_mul_ps(xx[3], c);

      t[0] = simde_mm_add_ps(t[0], b);
      t[1] = simde_mm_add_ps(t[1], b);
      t[2] = simde_mm_add_ps(t[2], b);
      t[3] = simde_mm_add_ps(t[3], b);

      t[0] = simde_mm_mul_ps(t[0], xx[0]);
      t[1] = simde_mm_mul_ps(t[1], xx[1]);
      t[2] = simde_mm_mul_ps(t[2], xx[2]);
      t[3] = simde_mm_mul_ps(t[3], xx[3]);

      t[0] = simde_mm_add_ps(t[0], a);
      t[1] = simde_mm_add_ps(t[1], a);
      t[2] = simde_mm_add_ps(t[2], a);
      t[3] = simde_mm_add_ps(t[3], a);

      t[0] = simde_mm_mul_ps(t[0], xx[0]);
      t[1] = simde_mm_mul_ps(t[1], xx[1]);
      t[2] = simde_mm_mul_ps(t[2], xx[2]);
      t[3] = simde_mm_mul_ps(t[3], xx[3]);

      t[0] = simde_mm_add_ps(t[0], one);
      t[1] = simde_mm_add_ps(t[1], one);
      t[2] = simde_mm_add_ps(t[2], one);
      t[3] = simde_mm_add_ps(t[3], one);

      t[0] = simde_mm_mul_ps(t[0], x[0]);
      t[1] = simde_mm_mul_ps(t[1], x[1]);
      t[2] = simde_mm_mul_ps(t[2], x[2]);
      t[3] = simde_mm_mul_ps(t[3], x[3]);

      ((simde__m128*)xb)[i] = t[0];
      ((simde__m128*)xb)[i + 1] = t[1];
      ((simde__m128*)xb)[i + 2] = t[2];
      ((simde__m128*)xb)[i + 3] = t[3];
   }
}

void clear_block(float* in, unsigned int nquads)
{
   const simde__m128 zero = simde_mm_set1_ps(0.f);

   for (unsigned int i = 0; i < nquads << 2; i += 4)
   {
      simde_mm_store_ps((float*)&in[i], zero);
   }
}

void clear_block_antidenormalnoise(float* in, unsigned int nquads)
{
   const simde__m128 smallvalue =
       simde_mm_set_ps(0.000000000000001f, 0.000000000000001f, -0.000000000000001f, -0.000000000000001f);

   for (unsigned int i = 0; i < (nquads << 2); i += 8)
   {
      simde_mm_store_ps((float*)&in[i], smallvalue);
      simde_mm_store_ps((float*)&in[i + 4], smallvalue);
   }
}

void accumulate_block(float* __restrict src,
                      float* __restrict dst,
                      unsigned int nquads) // dst += src
{
   for (unsigned int i = 0; i < nquads; i += 4)
   {
      ((simde__m128*)dst)[i] = simde_mm_add_ps(((simde__m128*)dst)[i], ((simde__m128*)src)[i]);
      ((simde__m128*)dst)[i + 1] = simde_mm_add_ps(((simde__m128*)dst)[i + 1], ((simde__m128*)src)[i + 1]);
      ((simde__m128*)dst)[i + 2] = simde_mm_add_ps(((simde__m128*)dst)[i + 2], ((simde__m128*)src)[i + 2]);
      ((simde__m128*)dst)[i + 3] = simde_mm_add_ps(((simde__m128*)dst)[i + 3], ((simde__m128*)src)[i + 3]);
   }
}

void copy_block(float* __restrict src, float* __restrict dst, unsigned int nquads)
{
   float *fdst, *fsrc;
   fdst = (float*)dst;
   fsrc = (float*)src;

   for (unsigned int i = 0; i < (nquads << 2); i += (8 << 2))
   {
      simde_mm_store_ps(&fdst[i], simde_mm_load_ps(&fsrc[i]));
      simde_mm_store_ps(&fdst[i + 4], simde_mm_load_ps(&fsrc[i + 4]));
      simde_mm_store_ps(&fdst[i + 8], simde_mm_load_ps(&fsrc[i + 8]));
      simde_mm_store_ps(&fdst[i + 12], simde_mm_load_ps(&fsrc[i + 12]));
      simde_mm_store_ps(&fdst[i + 16], simde_mm_load_ps(&fsrc[i + 16]));
      simde_mm_store_ps(&fdst[i + 20], simde_mm_load_ps(&fsrc[i + 20]));
      simde_mm_store_ps(&fdst[i + 24], simde_mm_load_ps(&fsrc[i + 24]));
      simde_mm_store_ps(&fdst[i + 28], simde_mm_load_ps(&fsrc[i + 28]));
   }
}

void copy_block_US(float* __restrict src, float* __restrict dst, unsigned int nquads)
{
   float *fdst, *fsrc;
   fdst = (float*)dst;
   fsrc = (float*)src;

   for (unsigned int i = 0; i < (nquads << 2); i += (8 << 2))
   {
      simde_mm_store_ps(&fdst[i], simde_mm_loadu_ps(&fsrc[i]));
      simde_mm_store_ps(&fdst[i + 4], simde_mm_loadu_ps(&fsrc[i + 4]));
      simde_mm_store_ps(&fdst[i + 8], simde_mm_loadu_ps(&fsrc[i + 8]));
      simde_mm_store_ps(&fdst[i + 12], simde_mm_loadu_ps(&fsrc[i + 12]));
      simde_mm_store_ps(&fdst[i + 16], simde_mm_loadu_ps(&fsrc[i + 16]));
      simde_mm_store_ps(&fdst[i + 20], simde_mm_loadu_ps(&fsrc[i + 20]));
      simde_mm_store_ps(&fdst[i + 24], simde_mm_loadu_ps(&fsrc[i + 24]));
      simde_mm_store_ps(&fdst[i + 28], simde_mm_loadu_ps(&fsrc[i + 28]));
   }
}

void copy_block_UD(float* __restrict src, float* __restrict dst, unsigned int nquads)
{
   float *fdst, *fsrc;
   fdst = (float*)dst;
   fsrc = (float*)src;

   for (unsigned int i = 0; i < (nquads << 2); i += (8 << 2))
   {
      simde_mm_storeu_ps(&fdst[i], simde_mm_load_ps(&fsrc[i]));
      simde_mm_storeu_ps(&fdst[i + 4], simde_mm_load_ps(&fsrc[i + 4]));
      simde_mm_storeu_ps(&fdst[i + 8], simde_mm_load_ps(&fsrc[i + 8]));
      simde_mm_storeu_ps(&fdst[i + 12], simde_mm_load_ps(&fsrc[i + 12]));
      simde_mm_storeu_ps(&fdst[i + 16], simde_mm_load_ps(&fsrc[i + 16]));
      simde_mm_storeu_ps(&fdst[i + 20], simde_mm_load_ps(&fsrc[i + 20]));
      simde_mm_storeu_ps(&fdst[i + 24], simde_mm_load_ps(&fsrc[i + 24]));
      simde_mm_storeu_ps(&fdst[i + 28], simde_mm_load_ps(&fsrc[i + 28]));
   }
}
void copy_block_USUD(float* __restrict src, float* __restrict dst, unsigned int nquads)
{
   float *fdst, *fsrc;
   fdst = (float*)dst;
   fsrc = (float*)src;

   for (unsigned int i = 0; i < (nquads << 2); i += (8 << 2))
   {
      simde_mm_storeu_ps(&fdst[i], simde_mm_loadu_ps(&fsrc[i]));
      simde_mm_storeu_ps(&fdst[i + 4], simde_mm_loadu_ps(&fsrc[i + 4]));
      simde_mm_storeu_ps(&fdst[i + 8], simde_mm_loadu_ps(&fsrc[i + 8]));
      simde_mm_storeu_ps(&fdst[i + 12], simde_mm_loadu_ps(&fsrc[i + 12]));
      simde_mm_storeu_ps(&fdst[i + 16], simde_mm_loadu_ps(&fsrc[i + 16]));
      simde_mm_storeu_ps(&fdst[i + 20], simde_mm_loadu_ps(&fsrc[i + 20]));
      simde_mm_storeu_ps(&fdst[i + 24], simde_mm_loadu_ps(&fsrc[i + 24]));
      simde_mm_storeu_ps(&fdst[i + 28], simde_mm_loadu_ps(&fsrc[i + 28]));
   }
}

void mul_block(float* __restrict src1,
               float* __restrict src2,
               float* __restrict dst,
               unsigned int nquads)
{
   for (unsigned int i = 0; i < nquads; i += 4)
   {
      ((simde__m128*)dst)[i] = simde_mm_mul_ps(((simde__m128*)src1)[i], ((simde__m128*)src2)[i]);
      ((simde__m128*)dst)[i + 1] = simde_mm_mul_ps(((simde__m128*)src1)[i + 1], ((simde__m128*)src2)[i + 1]);
      ((simde__m128*)dst)[i + 2] = simde_mm_mul_ps(((simde__m128*)src1)[i + 2], ((simde__m128*)src2)[i + 2]);
      ((simde__m128*)dst)[i + 3] = simde_mm_mul_ps(((simde__m128*)src1)[i + 3], ((simde__m128*)src2)[i + 3]);
   }
}

void encodeMS(float* __restrict L,
              float* __restrict R,
              float* __restrict M,
              float* __restrict S,
              unsigned int nquads)
{
   const simde__m128 half = simde_mm_set1_ps(0.5f);
#define L ((simde__m128*)L)
#define R ((simde__m128*)R)
#define M ((simde__m128*)M)
#define S ((simde__m128*)S)

   for (unsigned int i = 0; i < nquads; i += 4)
   {
      M[i] = simde_mm_mul_ps(simde_mm_add_ps(L[i], R[i]), half);
      S[i] = simde_mm_mul_ps(simde_mm_sub_ps(L[i], R[i]), half);
      M[i + 1] = simde_mm_mul_ps(simde_mm_add_ps(L[i + 1], R[i + 1]), half);
      S[i + 1] = simde_mm_mul_ps(simde_mm_sub_ps(L[i + 1], R[i + 1]), half);
      M[i + 2] = simde_mm_mul_ps(simde_mm_add_ps(L[i + 2], R[i + 2]), half);
      S[i + 2] = simde_mm_mul_ps(simde_mm_sub_ps(L[i + 2], R[i + 2]), half);
      M[i + 3] = simde_mm_mul_ps(simde_mm_add_ps(L[i + 3], R[i + 3]), half);
      S[i + 3] = simde_mm_mul_ps(simde_mm_sub_ps(L[i + 3], R[i + 3]), half);
   }
#undef L
#undef R
#undef M
#undef S
}
void decodeMS(float* __restrict M,
              float* __restrict S,
              float* __restrict L,
              float* __restrict R,
              unsigned int nquads)
{
#define L ((simde__m128*)L)
#define R ((simde__m128*)R)
#define M ((simde__m128*)M)
#define S ((simde__m128*)S)
   for (unsigned int i = 0; i < nquads; i += 4)
   {
      L[i] = simde_mm_add_ps(M[i], S[i]);
      R[i] = simde_mm_sub_ps(M[i], S[i]);
      L[i + 1] = simde_mm_add_ps(M[i + 1], S[i + 1]);
      R[i + 1] = simde_mm_sub_ps(M[i + 1], S[i + 1]);
      L[i + 2] = simde_mm_add_ps(M[i + 2], S[i + 2]);
      R[i + 2] = simde_mm_sub_ps(M[i + 2], S[i + 2]);
      L[i + 3] = simde_mm_add_ps(M[i + 3], S[i + 3]);
      R[i + 3] = simde_mm_sub_ps(M[i + 3], S[i + 3]);
   }
#undef L
#undef R
#undef M
#undef S
}

void add_block(float* __restrict src1,
               float* __restrict src2,
               float* __restrict dst,
               unsigned int nquads)
{
   for (unsigned int i = 0; i < nquads; i += 4)
   {
      ((simde__m128*)dst)[i] = simde_mm_add_ps(((simde__m128*)src1)[i], ((simde__m128*)src2)[i]);
      ((simde__m128*)dst)[i + 1] = simde_mm_add_ps(((simde__m128*)src1)[i + 1], ((simde__m128*)src2)[i + 1]);
      ((simde__m128*)dst)[i + 2] = simde_mm_add_ps(((simde__m128*)src1)[i + 2], ((simde__m128*)src2)[i + 2]);
      ((simde__m128*)dst)[i + 3] = simde_mm_add_ps(((simde__m128*)src1)[i + 3], ((simde__m128*)src2)[i + 3]);
   }
}

void subtract_block(float* __restrict src1,
                    float* __restrict src2,
                    float* __restrict dst,
                    unsigned int nquads)
{
   for (unsigned int i = 0; i < nquads; i += 4)
   {
      ((simde__m128*)dst)[i] = simde_mm_sub_ps(((simde__m128*)src1)[i], ((simde__m128*)src2)[i]);
      ((simde__m128*)dst)[i + 1] = simde_mm_sub_ps(((simde__m128*)src1)[i + 1], ((simde__m128*)src2)[i + 1]);
      ((simde__m128*)dst)[i + 2] = simde_mm_sub_ps(((simde__m128*)src1)[i + 2], ((simde__m128*)src2)[i + 2]);
      ((simde__m128*)dst)[i + 3] = simde_mm_sub_ps(((simde__m128*)src1)[i + 3], ((simde__m128*)src2)[i + 3]);
   }
}

// Snabba sin(x) substitut
// work in progress
inline float sine_float_nowrap(float x)
{
   // http://www.devmaster.net/forums/showthread.php?t=5784
   const float B = 4.f / M_PI;
   const float C = -4.f / (M_PI * M_PI);

   float y = B * x + C * x * ::abs(x);

   // EXTRA_PRECISION
   //  const float Q = 0.775;
   const float P = 0.225;

   return P * (y * fabs(y) - y) + y; // Q * y + P * y * abs(y)
}

inline simde__m128 sine_ps_nowrap(simde__m128 x)
{
   const simde__m128 B = simde_mm_set1_ps(4.f / M_PI);
   const simde__m128 C = simde_mm_set1_ps(-4.f / (M_PI * M_PI));

   // todo wrap x [0..1] ?

   // y = B * x + C * x * abs(x);
   simde__m128 y =
       simde_mm_add_ps(simde_mm_mul_ps(B, x), simde_mm_mul_ps(simde_mm_mul_ps(C, x), simde_mm_and_ps(m128_mask_absval, x)));

   // EXTRA_PRECISION
   //  const float Q = 0.775;
   const simde__m128 P = simde_mm_set1_ps(0.225f);
   return simde_mm_add_ps(simde_mm_mul_ps(simde_mm_sub_ps(simde_mm_mul_ps(simde_mm_and_ps(m128_mask_absval, y), y), y), P),
                     y);
}

inline simde__m128 sine_xpi_ps_SSE2(simde__m128 x) // sin(x*pi)
{
   const simde__m128 B = simde_mm_set1_ps(4.f);
   const simde__m128 premul = simde_mm_set1_ps(16777216.f);
   const simde__m128 postmul = simde_mm_set1_ps(1.f / 16777216.f);
   const simde__m128i mask = simde_mm_set1_epi32(0x01ffffff);
   const simde__m128i offset = simde_mm_set1_epi32(0x01000000);

   // wrap x
   x = simde_mm_cvtepi32_ps(
       simde_mm_sub_epi32(simde_mm_and_si128(simde_mm_add_epi32(offset, simde_mm_cvttps_epi32(x)), mask), offset));

   simde__m128 y = simde_mm_mul_ps(B, simde_mm_sub_ps(x, simde_mm_mul_ps(x, simde_mm_and_ps(m128_mask_absval, x))));

   const simde__m128 P = simde_mm_set1_ps(0.225f);
   return simde_mm_add_ps(simde_mm_mul_ps(simde_mm_sub_ps(simde_mm_mul_ps(simde_mm_and_ps(m128_mask_absval, y), y), y), P),
                     y);
}

float sine_ss(unsigned int x) // using 24-bit range as [0..2PI] input
{
   return 0.f;
   /*x = x & 0x00FFFFFF;	// wrap

   float fx;
   simde_mm_cvtsi32_ss

   const float B = 4.f/M_PI;
   const float C = -4.f/(M_PI*M_PI);

   float y = B * x + C * x * abs(x);

   //EXTRA_PRECISION
   //  const float Q = 0.775;
   const float P = 0.225;

   return P * (y * abs(y) - y) + y;   // Q * y + P * y * abs(y)   */
}
#if !_M_X64
simde__m64 sine(simde__m64 x)
{
   simde__m64 xabs = simde_mm_xor_si64(x, simde_mm_srai_pi16(x, 15));
   simde__m64 y = simde_mm_subs_pi16(simde_mm_srai_pi16(x, 1), simde_mm_mulhi_pi16(x, xabs));
   y = simde_mm_slli_pi16(y, 2);
   y = simde_mm_adds_pi16(y, y);
   const simde__m64 Q = simde_mm_set1_pi16(0x6333);
   const simde__m64 P = simde_mm_set1_pi16(0x1CCD);

   simde__m64 yabs = simde_mm_xor_si64(y, simde_mm_srai_pi16(y, 15));
   simde__m64 y1 = simde_mm_mulhi_pi16(Q, y);
   simde__m64 y2 = simde_mm_mulhi_pi16(P, simde_mm_slli_pi16(simde_mm_mulhi_pi16(y, yabs), 1));

   y = simde_mm_add_pi16(y1, y2);
   return simde_mm_adds_pi16(y, y);
}
#endif

int sine(int x) // 16-bit sine
{
   x = ((x + 0x8000) & 0xffff) - 0x8000;
   int y = ((x << 2) - ((abs(x >> 1) * (x >> 1)) >> 11));
   const int Q = (0.775f * 65536.f);
   const int P = (0.225f * 32768.f);
   y = ((Q * y) >> 16) + (((((y >> 2) * abs(y >> 2)) >> 11) * P) >> 15);
   return y;
}
