#pragma once

#include "globals.h"

inline float i2f_binary_cast(int i)
{
   float* f = (float*)&i;
   return *f;
};

const simde__m128 m128_mask_signbit = simde_mm_set1_ps(i2f_binary_cast(0x80000000));
const simde__m128 m128_mask_absval = simde_mm_set1_ps(i2f_binary_cast(0x7fffffff));
const simde__m128 m128_zero = simde_mm_set1_ps(0.0f);
const simde__m128 m128_half = simde_mm_set1_ps(0.5f);
const simde__m128 m128_one = simde_mm_set1_ps(1.0f);
const simde__m128 m128_two = simde_mm_set1_ps(2.0f);
const simde__m128 m128_four = simde_mm_set1_ps(4.0f);
const simde__m128 m128_1234 = simde_mm_set_ps(1.f, 2.f, 3.f, 4.f);
const simde__m128 m128_0123 = simde_mm_set_ps(0.f, 1.f, 2.f, 3.f);
