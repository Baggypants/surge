#include "QuadFilterUnit.h"
#include "SurgeStorage.h"
#include <vt_dsp/basic_dsp.h>

simde__m128 SVFLP12Aquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // F1
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // Q1

   simde__m128 L = simde_mm_add_ps(f->R[1], simde_mm_mul_ps(f->C[0], f->R[0]));
   simde__m128 H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], f->R[0]));
   simde__m128 B = simde_mm_add_ps(f->R[0], simde_mm_mul_ps(f->C[0], H));

   simde__m128 L2 = simde_mm_add_ps(L, simde_mm_mul_ps(f->C[0], B));
   simde__m128 H2 = simde_mm_sub_ps(simde_mm_sub_ps(in, L2), simde_mm_mul_ps(f->C[1], B));
   simde__m128 B2 = simde_mm_add_ps(B, simde_mm_mul_ps(f->C[0], H2));

   f->R[0] = simde_mm_mul_ps(B2, f->R[2]);
   f->R[1] = simde_mm_mul_ps(L2, f->R[2]);

   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]);
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[2], simde_mm_mul_ps(B, B))));

   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]); // Gain
   return simde_mm_mul_ps(L2, f->C[3]);
}

simde__m128 SVFLP24Aquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // F1
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // Q1

   simde__m128 L = simde_mm_add_ps(f->R[1], simde_mm_mul_ps(f->C[0], f->R[0]));
   simde__m128 H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], f->R[0]));
   simde__m128 B = simde_mm_add_ps(f->R[0], simde_mm_mul_ps(f->C[0], H));

   L = simde_mm_add_ps(L, simde_mm_mul_ps(f->C[0], B));
   H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], B));
   B = simde_mm_add_ps(B, simde_mm_mul_ps(f->C[0], H));

   f->R[0] = simde_mm_mul_ps(B, f->R[2]);
   f->R[1] = simde_mm_mul_ps(L, f->R[2]);

   in = L;

   L = simde_mm_add_ps(f->R[4], simde_mm_mul_ps(f->C[0], f->R[3]));
   H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], f->R[3]));
   B = simde_mm_add_ps(f->R[3], simde_mm_mul_ps(f->C[0], H));

   L = simde_mm_add_ps(L, simde_mm_mul_ps(f->C[0], B));
   H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], B));
   B = simde_mm_add_ps(B, simde_mm_mul_ps(f->C[0], H));

   f->R[3] = simde_mm_mul_ps(B, f->R[2]);
   f->R[4] = simde_mm_mul_ps(L, f->R[2]);

   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]);
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[2], simde_mm_mul_ps(B, B))));

   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]); // Gain
   return simde_mm_mul_ps(L, f->C[3]);
}

simde__m128 SVFHP24Aquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // F1
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // Q1

   simde__m128 L = simde_mm_add_ps(f->R[1], simde_mm_mul_ps(f->C[0], f->R[0]));
   simde__m128 H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], f->R[0]));
   simde__m128 B = simde_mm_add_ps(f->R[0], simde_mm_mul_ps(f->C[0], H));

   L = simde_mm_add_ps(L, simde_mm_mul_ps(f->C[0], B));
   H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], B));
   B = simde_mm_add_ps(B, simde_mm_mul_ps(f->C[0], H));

   f->R[0] = simde_mm_mul_ps(B, f->R[2]);
   f->R[1] = simde_mm_mul_ps(L, f->R[2]);

   in = H;

   L = simde_mm_add_ps(f->R[4], simde_mm_mul_ps(f->C[0], f->R[3]));
   H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], f->R[3]));
   B = simde_mm_add_ps(f->R[3], simde_mm_mul_ps(f->C[0], H));

   L = simde_mm_add_ps(L, simde_mm_mul_ps(f->C[0], B));
   H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], B));
   B = simde_mm_add_ps(B, simde_mm_mul_ps(f->C[0], H));

   f->R[3] = simde_mm_mul_ps(B, f->R[2]);
   f->R[4] = simde_mm_mul_ps(L, f->R[2]);

   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]);
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[2], simde_mm_mul_ps(B, B))));

   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]); // Gain
   return simde_mm_mul_ps(H, f->C[3]);
}

simde__m128 SVFBP24Aquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // F1
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // Q1

   simde__m128 L = simde_mm_add_ps(f->R[1], simde_mm_mul_ps(f->C[0], f->R[0]));
   simde__m128 H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], f->R[0]));
   simde__m128 B = simde_mm_add_ps(f->R[0], simde_mm_mul_ps(f->C[0], H));

   L = simde_mm_add_ps(L, simde_mm_mul_ps(f->C[0], B));
   H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], B));
   B = simde_mm_add_ps(B, simde_mm_mul_ps(f->C[0], H));

   f->R[0] = simde_mm_mul_ps(B, f->R[2]);
   f->R[1] = simde_mm_mul_ps(L, f->R[2]);

   in = B;

   L = simde_mm_add_ps(f->R[4], simde_mm_mul_ps(f->C[0], f->R[3]));
   H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], f->R[3]));
   B = simde_mm_add_ps(f->R[3], simde_mm_mul_ps(f->C[0], H));

   L = simde_mm_add_ps(L, simde_mm_mul_ps(f->C[0], B));
   H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], B));
   B = simde_mm_add_ps(B, simde_mm_mul_ps(f->C[0], H));

   f->R[3] = simde_mm_mul_ps(B, f->R[2]);
   f->R[4] = simde_mm_mul_ps(L, f->R[2]);

   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]);
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[2], simde_mm_mul_ps(B, B))));

   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]); // Gain
   return simde_mm_mul_ps(B, f->C[3]);
}

simde__m128 SVFHP12Aquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // F1
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // Q1

   simde__m128 L = simde_mm_add_ps(f->R[1], simde_mm_mul_ps(f->C[0], f->R[0]));
   simde__m128 H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], f->R[0]));
   simde__m128 B = simde_mm_add_ps(f->R[0], simde_mm_mul_ps(f->C[0], H));

   simde__m128 L2 = simde_mm_add_ps(L, simde_mm_mul_ps(f->C[0], B));
   simde__m128 H2 = simde_mm_sub_ps(simde_mm_sub_ps(in, L2), simde_mm_mul_ps(f->C[1], B));
   simde__m128 B2 = simde_mm_add_ps(B, simde_mm_mul_ps(f->C[0], H2));

   f->R[0] = simde_mm_mul_ps(B2, f->R[2]);
   f->R[1] = simde_mm_mul_ps(L2, f->R[2]);

   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]);
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[2], simde_mm_mul_ps(B, B))));

   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]); // Gain
   return simde_mm_mul_ps(H2, f->C[3]);
}

simde__m128 SVFBP12Aquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // F1
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // Q1

   simde__m128 L = simde_mm_add_ps(f->R[1], simde_mm_mul_ps(f->C[0], f->R[0]));
   simde__m128 H = simde_mm_sub_ps(simde_mm_sub_ps(in, L), simde_mm_mul_ps(f->C[1], f->R[0]));
   simde__m128 B = simde_mm_add_ps(f->R[0], simde_mm_mul_ps(f->C[0], H));

   simde__m128 L2 = simde_mm_add_ps(L, simde_mm_mul_ps(f->C[0], B));
   simde__m128 H2 = simde_mm_sub_ps(simde_mm_sub_ps(in, L2), simde_mm_mul_ps(f->C[1], B));
   simde__m128 B2 = simde_mm_add_ps(B, simde_mm_mul_ps(f->C[0], H2));

   f->R[0] = simde_mm_mul_ps(B2, f->R[2]);
   f->R[1] = simde_mm_mul_ps(L2, f->R[2]);

   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]);
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[2], simde_mm_mul_ps(B, B))));

   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]); // Gain
   return simde_mm_mul_ps(B2, f->C[3]);
}

simde__m128 IIR12Aquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]);                                       // K2
   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]);                                       // Q2
   simde__m128 f2 = simde_mm_sub_ps(simde_mm_mul_ps(f->C[3], in), simde_mm_mul_ps(f->C[1], f->R[1])); // Q2*in - K2*R1
   simde__m128 g2 = simde_mm_add_ps(simde_mm_mul_ps(f->C[1], in), simde_mm_mul_ps(f->C[3], f->R[1])); // K2*in + Q2*R1

   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]);                                       // K1
   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]);                                       // Q1
   simde__m128 f1 = simde_mm_sub_ps(simde_mm_mul_ps(f->C[2], f2), simde_mm_mul_ps(f->C[0], f->R[0])); // Q1*f2 - K1*R0
   simde__m128 g1 = simde_mm_add_ps(simde_mm_mul_ps(f->C[0], f2), simde_mm_mul_ps(f->C[2], f->R[0])); // K1*f2 + Q1*R0

   f->C[4] = simde_mm_add_ps(f->C[4], f->dC[4]); // V1
   f->C[5] = simde_mm_add_ps(f->C[5], f->dC[5]); // V2
   f->C[6] = simde_mm_add_ps(f->C[6], f->dC[6]); // V3
   simde__m128 y = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[6], g2), simde_mm_mul_ps(f->C[5], g1)),
                         simde_mm_mul_ps(f->C[4], f1));

   f->R[0] = f1;
   f->R[1] = g1;

   return y;
}

simde__m128 IIR12Bquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   simde__m128 f2 = simde_mm_sub_ps(simde_mm_mul_ps(f->C[3], in), simde_mm_mul_ps(f->C[1], f->R[1])); // Q2*in - K2*R1
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]);                                       // K2
   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]);                                       // Q2
   simde__m128 g2 = simde_mm_add_ps(simde_mm_mul_ps(f->C[1], in), simde_mm_mul_ps(f->C[3], f->R[1])); // K2*in + Q2*R1

   simde__m128 f1 = simde_mm_sub_ps(simde_mm_mul_ps(f->C[2], f2), simde_mm_mul_ps(f->C[0], f->R[0])); // Q1*f2 - K1*R0
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]);                                       // K1
   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]);                                       // Q1
   simde__m128 g1 = simde_mm_add_ps(simde_mm_mul_ps(f->C[0], f2), simde_mm_mul_ps(f->C[2], f->R[0])); // K1*f2 + Q1*R0

   f->C[4] = simde_mm_add_ps(f->C[4], f->dC[4]); // V1
   f->C[5] = simde_mm_add_ps(f->C[5], f->dC[5]); // V2
   f->C[6] = simde_mm_add_ps(f->C[6], f->dC[6]); // V3
   simde__m128 y = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[6], g2), simde_mm_mul_ps(f->C[5], g1)),
                         simde_mm_mul_ps(f->C[4], f1));

   f->R[0] = simde_mm_mul_ps(f1, f->R[2]);
   f->R[1] = simde_mm_mul_ps(g1, f->R[2]);

   f->C[7] = simde_mm_add_ps(f->C[7], f->dC[7]); // Clipgain
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);

   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[7], simde_mm_mul_ps(y, y))));

   return y;
}

simde__m128 IIR12WDFquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // E1 * sc
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // E2 * sc
   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]); // -E1 / sc
   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]); // -E2 / sc
   f->C[4] = simde_mm_add_ps(f->C[4], f->dC[4]); // C1
   f->C[5] = simde_mm_add_ps(f->C[5], f->dC[5]); // C2
   f->C[6] = simde_mm_add_ps(f->C[6], f->dC[6]); // D

   simde__m128 y = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[4], f->R[0]), simde_mm_mul_ps(f->C[6], in)),
                         simde_mm_mul_ps(f->C[5], f->R[1]));
   simde__m128 t =
       simde_mm_add_ps(in, simde_mm_add_ps(simde_mm_mul_ps(f->C[2], f->R[0]), simde_mm_mul_ps(f->C[3], f->R[1])));

   simde__m128 s1 = simde_mm_add_ps(simde_mm_mul_ps(t, f->C[0]), f->R[0]);
   simde__m128 s2 = simde_mm_sub_ps(simde_mm_setzero_ps(), simde_mm_add_ps(simde_mm_mul_ps(t, f->C[1]), f->R[1]));

   // f->R[0] = s1;
   // f->R[1] = s2;

   f->R[0] = simde_mm_mul_ps(s1, f->R[2]);
   f->R[1] = simde_mm_mul_ps(s2, f->R[2]);

   f->C[7] = simde_mm_add_ps(f->C[7], f->dC[7]); // Clipgain
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[7], simde_mm_mul_ps(y, y))));

   return y;
}

simde__m128 IIR12CFCquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   // State-space with clipgain (2nd order, limit within register)

   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // ar
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // ai
   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]); // b1
   f->C[4] = simde_mm_add_ps(f->C[4], f->dC[4]); // c1
   f->C[5] = simde_mm_add_ps(f->C[5], f->dC[5]); // c2
   f->C[6] = simde_mm_add_ps(f->C[6], f->dC[6]); // d

   // y(i) = c1.*s(1) + c2.*s(2) + d.*x(i);
   // s1 = ar.*s(1) - ai.*s(2) + x(i);
   // s2 = ai.*s(1) + ar.*s(2);

   simde__m128 y = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[4], f->R[0]), simde_mm_mul_ps(f->C[6], in)),
                         simde_mm_mul_ps(f->C[5], f->R[1]));
   simde__m128 s1 = simde_mm_add_ps(simde_mm_mul_ps(in, f->C[2]),
                          simde_mm_sub_ps(simde_mm_mul_ps(f->C[0], f->R[0]), simde_mm_mul_ps(f->C[1], f->R[1])));
   simde__m128 s2 = simde_mm_add_ps(simde_mm_mul_ps(f->C[1], f->R[0]), simde_mm_mul_ps(f->C[0], f->R[1]));

   f->R[0] = simde_mm_mul_ps(s1, f->R[2]);
   f->R[1] = simde_mm_mul_ps(s2, f->R[2]);

   f->C[7] = simde_mm_add_ps(f->C[7], f->dC[7]); // Clipgain
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[7], simde_mm_mul_ps(y, y))));

   return y;
}

simde__m128 IIR12CFLquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   // State-space with softer limiter

   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // (ar)
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // (ai)
   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]); // b1
   f->C[4] = simde_mm_add_ps(f->C[4], f->dC[4]); // c1
   f->C[5] = simde_mm_add_ps(f->C[5], f->dC[5]); // c2
   f->C[6] = simde_mm_add_ps(f->C[6], f->dC[6]); // d

   // y(i) = c1.*s(1) + c2.*s(2) + d.*x(i);
   // s1 = ar.*s(1) - ai.*s(2) + x(i);
   // s2 = ai.*s(1) + ar.*s(2);

   simde__m128 y = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[4], f->R[0]), simde_mm_mul_ps(f->C[6], in)),
                         simde_mm_mul_ps(f->C[5], f->R[1]));
   simde__m128 ar = simde_mm_mul_ps(f->C[0], f->R[2]);
   simde__m128 ai = simde_mm_mul_ps(f->C[1], f->R[2]);
   simde__m128 s1 = simde_mm_add_ps(simde_mm_mul_ps(in, f->C[2]),
                          simde_mm_sub_ps(simde_mm_mul_ps(ar, f->R[0]), simde_mm_mul_ps(ai, f->R[1])));
   simde__m128 s2 = simde_mm_add_ps(simde_mm_mul_ps(ai, f->R[0]), simde_mm_mul_ps(ar, f->R[1]));

   f->R[0] = s1;
   f->R[1] = s2;

   /*m = 1 ./ max(1,abs(y(i)));
   mr = mr.*0.99 + m.*0.01;*/

   // Limiter
   const simde__m128 m001 = simde_mm_set1_ps(0.001f);
   const simde__m128 m099 = simde_mm_set1_ps(0.999f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   const simde__m128 m2 = simde_mm_set1_ps(2.0f);

   simde__m128 m = simde_mm_rsqrt_ps(simde_mm_max_ps(m1, simde_mm_mul_ps(m2, simde_mm_and_ps(y, m128_mask_absval))));
   f->R[2] = simde_mm_add_ps(simde_mm_mul_ps(f->R[2], m099), simde_mm_mul_ps(m, m001));

   return y;
}

simde__m128 IIR24CFCquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   // State-space with clipgain (2nd order, limit within register)

   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // ar
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // ai
   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]); // b1

   f->C[4] = simde_mm_add_ps(f->C[4], f->dC[4]); // c1
   f->C[5] = simde_mm_add_ps(f->C[5], f->dC[5]); // c2
   f->C[6] = simde_mm_add_ps(f->C[6], f->dC[6]); // d

   simde__m128 y = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[4], f->R[0]), simde_mm_mul_ps(f->C[6], in)),
                         simde_mm_mul_ps(f->C[5], f->R[1]));
   simde__m128 s1 = simde_mm_add_ps(simde_mm_mul_ps(in, f->C[2]),
                          simde_mm_sub_ps(simde_mm_mul_ps(f->C[0], f->R[0]), simde_mm_mul_ps(f->C[1], f->R[1])));
   simde__m128 s2 = simde_mm_add_ps(simde_mm_mul_ps(f->C[1], f->R[0]), simde_mm_mul_ps(f->C[0], f->R[1]));

   f->R[0] = simde_mm_mul_ps(s1, f->R[2]);
   f->R[1] = simde_mm_mul_ps(s2, f->R[2]);

   simde__m128 y2 = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[4], f->R[3]), simde_mm_mul_ps(f->C[6], y)),
                          simde_mm_mul_ps(f->C[5], f->R[4]));
   simde__m128 s3 = simde_mm_add_ps(simde_mm_mul_ps(y, f->C[2]),
                          simde_mm_sub_ps(simde_mm_mul_ps(f->C[0], f->R[3]), simde_mm_mul_ps(f->C[1], f->R[4])));
   simde__m128 s4 = simde_mm_add_ps(simde_mm_mul_ps(f->C[1], f->R[3]), simde_mm_mul_ps(f->C[0], f->R[4]));

   f->R[3] = simde_mm_mul_ps(s3, f->R[2]);
   f->R[4] = simde_mm_mul_ps(s4, f->R[2]);

   f->C[7] = simde_mm_add_ps(f->C[7], f->dC[7]); // Clipgain
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[2] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[7], simde_mm_mul_ps(y2, y2))));

   return y2;
}

simde__m128 IIR24CFLquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   // State-space with softer limiter

   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // (ar)
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // (ai)
   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]); // b1
   f->C[4] = simde_mm_add_ps(f->C[4], f->dC[4]); // c1
   f->C[5] = simde_mm_add_ps(f->C[5], f->dC[5]); // c2
   f->C[6] = simde_mm_add_ps(f->C[6], f->dC[6]); // d

   simde__m128 ar = simde_mm_mul_ps(f->C[0], f->R[2]);
   simde__m128 ai = simde_mm_mul_ps(f->C[1], f->R[2]);

   simde__m128 y = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[4], f->R[0]), simde_mm_mul_ps(f->C[6], in)),
                         simde_mm_mul_ps(f->C[5], f->R[1]));
   simde__m128 s1 = simde_mm_add_ps(simde_mm_mul_ps(in, f->C[2]),
                          simde_mm_sub_ps(simde_mm_mul_ps(ar, f->R[0]), simde_mm_mul_ps(ai, f->R[1])));
   simde__m128 s2 = simde_mm_add_ps(simde_mm_mul_ps(ai, f->R[0]), simde_mm_mul_ps(ar, f->R[1]));

   f->R[0] = s1;
   f->R[1] = s2;

   simde__m128 y2 = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[4], f->R[3]), simde_mm_mul_ps(f->C[6], y)),
                          simde_mm_mul_ps(f->C[5], f->R[4]));
   simde__m128 s3 = simde_mm_add_ps(simde_mm_mul_ps(y, f->C[2]),
                          simde_mm_sub_ps(simde_mm_mul_ps(ar, f->R[3]), simde_mm_mul_ps(ai, f->R[4])));
   simde__m128 s4 = simde_mm_add_ps(simde_mm_mul_ps(ai, f->R[3]), simde_mm_mul_ps(ar, f->R[4]));

   f->R[3] = s3;
   f->R[4] = s4;

   /*m = 1 ./ max(1,abs(y(i)));
   mr = mr.*0.99 + m.*0.01;*/

   // Limiter
   const simde__m128 m001 = simde_mm_set1_ps(0.001f);
   const simde__m128 m099 = simde_mm_set1_ps(0.999f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   const simde__m128 m2 = simde_mm_set1_ps(2.0f);

   simde__m128 m = simde_mm_rsqrt_ps(simde_mm_max_ps(m1, simde_mm_mul_ps(m2, simde_mm_and_ps(y2, m128_mask_absval))));
   f->R[2] = simde_mm_add_ps(simde_mm_mul_ps(f->R[2], m099), simde_mm_mul_ps(m, m001));

   return y2;
}

simde__m128 IIR24Bquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]); // K2
   f->C[3] = simde_mm_add_ps(f->C[3], f->dC[3]); // Q2
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]); // K1
   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]); // Q1
   f->C[4] = simde_mm_add_ps(f->C[4], f->dC[4]); // V1
   f->C[5] = simde_mm_add_ps(f->C[5], f->dC[5]); // V2
   f->C[6] = simde_mm_add_ps(f->C[6], f->dC[6]); // V3

   simde__m128 f2 = simde_mm_sub_ps(simde_mm_mul_ps(f->C[3], in), simde_mm_mul_ps(f->C[1], f->R[1])); // Q2*in - K2*R1
   simde__m128 g2 = simde_mm_add_ps(simde_mm_mul_ps(f->C[1], in), simde_mm_mul_ps(f->C[3], f->R[1])); // K2*in + Q2*R1
   simde__m128 f1 = simde_mm_sub_ps(simde_mm_mul_ps(f->C[2], f2), simde_mm_mul_ps(f->C[0], f->R[0])); // Q1*f2 - K1*R0
   simde__m128 g1 = simde_mm_add_ps(simde_mm_mul_ps(f->C[0], f2), simde_mm_mul_ps(f->C[2], f->R[0])); // K1*f2 + Q1*R0
   f->R[0] = simde_mm_mul_ps(f1, f->R[4]);
   f->R[1] = simde_mm_mul_ps(g1, f->R[4]);
   simde__m128 y1 = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[6], g2), simde_mm_mul_ps(f->C[5], g1)),
                          simde_mm_mul_ps(f->C[4], f1));

   f2 = simde_mm_sub_ps(simde_mm_mul_ps(f->C[3], y1), simde_mm_mul_ps(f->C[1], f->R[3])); // Q2*in - K2*R1
   g2 = simde_mm_add_ps(simde_mm_mul_ps(f->C[1], y1), simde_mm_mul_ps(f->C[3], f->R[3])); // K2*in + Q2*R1
   f1 = simde_mm_sub_ps(simde_mm_mul_ps(f->C[2], f2), simde_mm_mul_ps(f->C[0], f->R[2])); // Q1*f2 - K1*R0
   g1 = simde_mm_add_ps(simde_mm_mul_ps(f->C[0], f2), simde_mm_mul_ps(f->C[2], f->R[2])); // K1*f2 + Q1*R0
   f->R[2] = simde_mm_mul_ps(f1, f->R[4]);
   f->R[3] = simde_mm_mul_ps(g1, f->R[4]);
   simde__m128 y2 = simde_mm_add_ps(simde_mm_add_ps(simde_mm_mul_ps(f->C[6], g2), simde_mm_mul_ps(f->C[5], g1)),
                          simde_mm_mul_ps(f->C[4], f1));

   f->C[7] = simde_mm_add_ps(f->C[7], f->dC[7]); // Clipgain
   const simde__m128 m01 = simde_mm_set1_ps(0.1f);
   const simde__m128 m1 = simde_mm_set1_ps(1.0f);
   f->R[4] = simde_mm_max_ps(m01, simde_mm_sub_ps(m1, simde_mm_mul_ps(f->C[7], simde_mm_mul_ps(y2, y2))));

   return y2;
}

simde__m128 LPMOOGquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]);
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]);
   f->C[2] = simde_mm_add_ps(f->C[2], f->dC[2]);

   f->R[0] = softclip8_ps(simde_mm_add_ps(
       f->R[0],
       simde_mm_mul_ps(f->C[1], simde_mm_sub_ps(simde_mm_sub_ps(simde_mm_mul_ps(in, f->C[0]),
                                                 simde_mm_mul_ps(f->C[2], simde_mm_add_ps(f->R[3], f->R[4]))),
                                      f->R[0]))));
   f->R[1] = simde_mm_add_ps(f->R[1], simde_mm_mul_ps(f->C[1], simde_mm_sub_ps(f->R[0], f->R[1])));
   f->R[2] = simde_mm_add_ps(f->R[2], simde_mm_mul_ps(f->C[1], simde_mm_sub_ps(f->R[1], f->R[2])));
   f->R[4] = f->R[3];
   f->R[3] = simde_mm_add_ps(f->R[3], simde_mm_mul_ps(f->C[1], simde_mm_sub_ps(f->R[2], f->R[3])));

   return f->R[f->WP[0] & 3];
}

simde__m128 SNHquad(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]);
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]);

   f->R[0] = simde_mm_add_ps(f->R[0], f->C[0]);

   simde__m128 mask = simde_mm_cmpgt_ps(f->R[0], simde_mm_setzero_ps());

   f->R[1] = simde_mm_or_ps(simde_mm_andnot_ps(mask, f->R[1]),
                       simde_mm_and_ps(mask, softclip_ps(simde_mm_sub_ps(in, simde_mm_mul_ps(f->C[1], f->R[1])))));

   const simde__m128 m1 = simde_mm_set1_ps(-1.f);
   f->R[0] = simde_mm_add_ps(f->R[0], simde_mm_and_ps(m1, mask));

   return f->R[1];
}

#if !_M_X64
simde__m128 COMBquad_SSE1(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   assert(FIRipol_M == 256); // changing the constant requires updating the code below
   const simde__m128 m256 = simde_mm_set1_ps(256.f);
   const simde__m128i m0xff = simde_mm_set1_epi32(0xff);

   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]);
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]);

   simde__m128 a = simde_mm_mul_ps(f->C[0], m256);
   simde__m128 DBRead = simde_mm_setzero_ps();

   for (int i = 0; i < 4; i++)
   {
      if (f->active[i])
      {
         int e = simde_mm_cvtss_si32(simde_mm_load_ss((float*)&a + i));
         int DT = e >> 8;
         int SE = (0xff - (e & 0xff)) * (FIRipol_N << 1);

         int RP = (f->WP[i] - DT - FIRoffset) & (MAX_FB_COMB - 1);

         // SINC interpolation (12 samples)
         simde__m128 a = simde_mm_loadu_ps(&f->DB[i][RP]);
         simde__m128 b = simde_mm_load_ps(&sinctable[SE]);
         simde__m128 o = simde_mm_mul_ps(a, b);

         a = simde_mm_loadu_ps(&f->DB[i][RP + 4]);
         b = simde_mm_load_ps(&sinctable[SE + 4]);
         o = simde_mm_add_ps(o, simde_mm_mul_ps(a, b));

         a = simde_mm_loadu_ps(&f->DB[i][RP + 8]);
         b = simde_mm_load_ps(&sinctable[SE + 8]);
         o = simde_mm_add_ps(o, simde_mm_mul_ps(a, b));

         simde_mm_store_ss((float*)&DBRead + i, sum_ps_to_ss(o));
      }
   }

   simde__m128 d = simde_mm_add_ps(in, simde_mm_mul_ps(DBRead, f->C[1]));
   d = softclip_ps(d);

   for (int i = 0; i < 4; i++)
   {
      if (f->active[i])
      {
         // Write to delaybuffer (with "anti-wrapping")
         simde__m128 t = simde_mm_load_ss((float*)&d + i);
         simde_mm_store_ss(&f->DB[i][f->WP[i]], t);
         if (f->WP[i] < FIRipol_N)
            simde_mm_store_ss(&f->DB[i][f->WP[i] + MAX_FB_COMB], t);

         // Increment write position
         f->WP[i] = (f->WP[i] + 1) & (MAX_FB_COMB - 1);
      }
   }
   return simde_mm_add_ps(simde_mm_mul_ps(f->C[3], DBRead), simde_mm_mul_ps(f->C[2], in));
}
#endif

simde__m128 COMBquad_SSE2(QuadFilterUnitState* __restrict f, simde__m128 in)
{
   assert(FIRipol_M == 256); // changing the constant requires updating the code below
   const simde__m128 m256 = simde_mm_set1_ps(256.f);
   const simde__m128i m0xff = simde_mm_set1_epi32(0xff);

   f->C[0] = simde_mm_add_ps(f->C[0], f->dC[0]);
   f->C[1] = simde_mm_add_ps(f->C[1], f->dC[1]);

   simde__m128 a = simde_mm_mul_ps(f->C[0], m256);
   simde__m128i e = simde_mm_cvtps_epi32(a);
   int DTi alignas(16)[4],
       SEi alignas(16)[4];
   simde__m128i DT = simde_mm_srli_epi32(e, 8);
   simde_mm_store_si128((simde__m128i*)DTi, DT);
   simde__m128i SE = simde_mm_and_si128(e, m0xff);
   SE = simde_mm_sub_epi32(m0xff, SE);
   simde_mm_store_si128((simde__m128i*)SEi, SE);
   simde__m128 DBRead = simde_mm_setzero_ps();

   for (int i = 0; i < 4; i++)
   {
      if (f->active[i])
      {
         int RP = (f->WP[i] - DTi[i] - FIRoffset) & (MAX_FB_COMB - 1);

         // SINC interpolation (12 samples)
         simde__m128 a = simde_mm_loadu_ps(&f->DB[i][RP]);
         SEi[i] *= (FIRipol_N << 1);
         simde__m128 b = simde_mm_load_ps(&sinctable[SEi[i]]);
         simde__m128 o = simde_mm_mul_ps(a, b);

         a = simde_mm_loadu_ps(&f->DB[i][RP + 4]);
         b = simde_mm_load_ps(&sinctable[SEi[i] + 4]);
         o = simde_mm_add_ps(o, simde_mm_mul_ps(a, b));

         a = simde_mm_loadu_ps(&f->DB[i][RP + 8]);
         b = simde_mm_load_ps(&sinctable[SEi[i] + 8]);
         o = simde_mm_add_ps(o, simde_mm_mul_ps(a, b));

         simde_mm_store_ss((float*)&DBRead + i, sum_ps_to_ss(o));
      }
   }

   simde__m128 d = simde_mm_add_ps(in, simde_mm_mul_ps(DBRead, f->C[1]));
   d = softclip_ps(d);

   for (int i = 0; i < 4; i++)
   {
      if (f->active[i])
      {
         // Write to delaybuffer (with "anti-wrapping")
         simde__m128 t = simde_mm_load_ss((float*)&d + i);
         simde_mm_store_ss(&f->DB[i][f->WP[i]], t);
         if (f->WP[i] < FIRipol_N)
            simde_mm_store_ss(&f->DB[i][f->WP[i] + MAX_FB_COMB], t);

         // Increment write position
         f->WP[i] = (f->WP[i] + 1) & (MAX_FB_COMB - 1);
      }
   }
   return simde_mm_add_ps(simde_mm_mul_ps(f->C[3], DBRead), simde_mm_mul_ps(f->C[2], in));
}

simde__m128 CLIP(simde__m128 in, simde__m128 drive)
{
   const simde__m128 x_min = simde_mm_set1_ps(-1.0f);
   const simde__m128 x_max = simde_mm_set1_ps(1.0f);
   return simde_mm_max_ps(simde_mm_min_ps(simde_mm_mul_ps(in, drive), x_max), x_min);
}

simde__m128 DIGI_SSE2(simde__m128 in, simde__m128 drive)
{
   // v1.2: return (double)((int)((double)(x*p0inv*16.f+1.0)))*p0*0.0625f;
   const simde__m128 m16 = simde_mm_set1_ps(16.f);
   const simde__m128 m16inv = simde_mm_set1_ps(0.0625f);
   const simde__m128 mofs = simde_mm_set1_ps(0.5f);

   simde__m128 invdrive = simde_mm_rcp_ps(drive);
   simde__m128i a = simde_mm_cvtps_epi32(simde_mm_add_ps(mofs, simde_mm_mul_ps(invdrive, simde_mm_mul_ps(m16, in))));

   return simde_mm_mul_ps(drive, simde_mm_mul_ps(m16inv, simde_mm_sub_ps(simde_mm_cvtepi32_ps(a), mofs)));
}

#if !_M_X64
simde__m128 DIGI_SSE1(simde__m128 in, simde__m128 drive)
{
   const simde__m128 mofs = simde_mm_set1_ps(0.5f);
   const simde__m128 m16 = simde_mm_set1_ps(16.f);
   const simde__m128 m16inv = simde_mm_set1_ps(0.0625f);

   simde__m128 invdrive = simde_mm_rcp_ps(drive);
   simde__m128 a = simde_mm_add_ps(mofs, simde_mm_mul_ps(invdrive, simde_mm_mul_ps(m16, in)));
   simde__m64 a1 = simde_mm_cvtps_pi32(a);
   simde__m64 a2 = simde_mm_cvtps_pi32(simde_mm_movehl_ps(a, a));
   a = simde_mm_cvtpi32_ps(simde_mm_movelh_ps(a, simde_mm_cvtpi32_ps(a, a2)), a1);
   simde__m128 b = simde_mm_mul_ps(drive, simde_mm_mul_ps(m16inv, simde_mm_sub_ps(a, mofs)));
   simde_mm_empty();
   return b;
}
#endif

simde__m128 TANH(simde__m128 in, simde__m128 drive)
{
   // Closer to ideal than TANH0
   // y = x * ( 27 + x * x ) / ( 27 + 9 * x * x );
   // y = clip(y)

   const simde__m128 m9 = simde_mm_set1_ps(9.f);
   const simde__m128 m27 = simde_mm_set1_ps(27.f);

   simde__m128 x = simde_mm_mul_ps(in, drive);
   simde__m128 xx = simde_mm_mul_ps(x, x);
   simde__m128 denom = simde_mm_add_ps(m27, simde_mm_mul_ps(m9, xx));
   simde__m128 y = simde_mm_mul_ps(x, simde_mm_add_ps(m27, xx));
   y = simde_mm_mul_ps(y, simde_mm_rcp_ps(denom));

   const simde__m128 y_min = simde_mm_set1_ps(-1.0f);
   const simde__m128 y_max = simde_mm_set1_ps(1.0f);
   return simde_mm_max_ps(simde_mm_min_ps(y, y_max), y_min);
}

#if !_M_X64
simde__m128 SINUS_SSE1(simde__m128 in, simde__m128 drive)
{
   const simde__m128 one = simde_mm_set1_ps(1.f);
   const simde__m128 m256 = simde_mm_set1_ps(256.f);
   const simde__m128 m512 = simde_mm_set1_ps(512.f);

   simde__m128 x = simde_mm_mul_ps(in, drive);
   x = simde_mm_add_ps(simde_mm_mul_ps(x, m256), m512);

   simde__m64 e1 = simde_mm_cvtps_pi32(x);
   simde__m64 e2 = simde_mm_cvtps_pi32(simde_mm_movehl_ps(x, x));
   simde__m128 a = simde_mm_sub_ps(x, simde_mm_cvtpi32_ps(simde_mm_movelh_ps(x, simde_mm_cvtpi32_ps(x, e2)), e1));
   int e11 = *(int*)&e1;
   int e12 = *((int*)&e1 + 1);
   int e21 = *(int*)&e2;
   int e22 = *((int*)&e2 + 1);

   simde__m128 ws1 = simde_mm_load_ss(&waveshapers[3][e11 & 0x3ff]);
   simde__m128 ws2 = simde_mm_load_ss(&waveshapers[3][e12 & 0x3ff]);
   simde__m128 ws3 = simde_mm_load_ss(&waveshapers[3][e21 & 0x3ff]);
   simde__m128 ws4 = simde_mm_load_ss(&waveshapers[3][e22 & 0x3ff]);
   simde__m128 ws = simde_mm_movelh_ps(simde_mm_unpacklo_ps(ws1, ws2), simde_mm_unpacklo_ps(ws3, ws4));
   ws1 = simde_mm_load_ss(&waveshapers[3][(e11 + 1) & 0x3ff]);
   ws2 = simde_mm_load_ss(&waveshapers[3][(e12 + 1) & 0x3ff]);
   ws3 = simde_mm_load_ss(&waveshapers[3][(e21 + 1) & 0x3ff]);
   ws4 = simde_mm_load_ss(&waveshapers[3][(e22 + 1) & 0x3ff]);
   simde__m128 wsn = simde_mm_movelh_ps(simde_mm_unpacklo_ps(ws1, ws2), simde_mm_unpacklo_ps(ws3, ws4));

   x = simde_mm_add_ps(simde_mm_mul_ps(simde_mm_sub_ps(one, a), ws), simde_mm_mul_ps(a, wsn));

   simde_mm_empty();

   return x;
}
#endif
simde__m128 SINUS_SSE2(simde__m128 in, simde__m128 drive)
{
   const simde__m128 one = simde_mm_set1_ps(1.f);
   const simde__m128 m256 = simde_mm_set1_ps(256.f);
   const simde__m128 m512 = simde_mm_set1_ps(512.f);

   simde__m128 x = simde_mm_mul_ps(in, drive);
   x = simde_mm_add_ps(simde_mm_mul_ps(x, m256), m512);

   simde__m128i e = simde_mm_cvtps_epi32(x);
   simde__m128 a = simde_mm_sub_ps(x, simde_mm_cvtepi32_ps(e));
   e = simde_mm_packs_epi32(e, e);

#if MAC
   // this should be very fast on C2D/C1D (and there are no macs with K8's)
   // GCC seems to optimize around the XMM -> int transfers so this is needed here
   int e4 alignas(16)[4];
   e4[0] = simde_mm_cvtsi128_si32(e);
   e4[1] = simde_mm_cvtsi128_si32(simde_mm_shufflelo_epi16(e, _MM_SHUFFLE(1, 1, 1, 1)));
   e4[2] = simde_mm_cvtsi128_si32(simde_mm_shufflelo_epi16(e, _MM_SHUFFLE(2, 2, 2, 2)));
   e4[3] = simde_mm_cvtsi128_si32(simde_mm_shufflelo_epi16(e, _MM_SHUFFLE(3, 3, 3, 3)));
#else
   // on PC write to memory & back as XMM -> GPR is slow on K8
   short e4 alignas(16)[8];
   simde_mm_store_si128((simde__m128i*)&e4, e);
#endif

   simde__m128 ws1 = simde_mm_load_ss(&waveshapers[3][e4[0] & 0x3ff]);
   simde__m128 ws2 = simde_mm_load_ss(&waveshapers[3][e4[1] & 0x3ff]);
   simde__m128 ws3 = simde_mm_load_ss(&waveshapers[3][e4[2] & 0x3ff]);
   simde__m128 ws4 = simde_mm_load_ss(&waveshapers[3][e4[3] & 0x3ff]);
   simde__m128 ws = simde_mm_movelh_ps(simde_mm_unpacklo_ps(ws1, ws2), simde_mm_unpacklo_ps(ws3, ws4));
   ws1 = simde_mm_load_ss(&waveshapers[3][(e4[0] + 1) & 0x3ff]);
   ws2 = simde_mm_load_ss(&waveshapers[3][(e4[1] + 1) & 0x3ff]);
   ws3 = simde_mm_load_ss(&waveshapers[3][(e4[2] + 1) & 0x3ff]);
   ws4 = simde_mm_load_ss(&waveshapers[3][(e4[3] + 1) & 0x3ff]);
   simde__m128 wsn = simde_mm_movelh_ps(simde_mm_unpacklo_ps(ws1, ws2), simde_mm_unpacklo_ps(ws3, ws4));

   x = simde_mm_add_ps(simde_mm_mul_ps(simde_mm_sub_ps(one, a), ws), simde_mm_mul_ps(a, wsn));

   return x;
}

#if !_M_X64
simde__m128 ASYM_SSE1(simde__m128 in, simde__m128 drive)
{
   const simde__m128 one = simde_mm_set1_ps(1.f);
   const simde__m128 m32 = simde_mm_set1_ps(32.f);
   const simde__m128 m512 = simde_mm_set1_ps(512.f);
   const simde__m64 LB = simde_mm_set1_pi16(0);
   const simde__m64 UB = simde_mm_set1_pi16(0x3fe);

   simde__m128 x = simde_mm_mul_ps(in, drive);
   x = simde_mm_add_ps(simde_mm_mul_ps(x, m32), m512);

   simde__m64 e1 = simde_mm_cvtps_pi32(x);
   simde__m64 e2 = simde_mm_cvtps_pi32(simde_mm_movehl_ps(x, x));
   simde__m128 a = simde_mm_sub_ps(x, simde_mm_cvtpi32_ps(simde_mm_movelh_ps(x, simde_mm_cvtpi32_ps(x, e2)), e1));

   e1 = simde_mm_packs_pi32(e1, e2);
   e1 = simde_mm_max_pi16(simde_mm_min_pi16(e1, UB), LB);

   int e11 = *(int*)&e1;
   int e12 = *((int*)&e1 + 1);
   int e21 = *(int*)&e2;
   int e22 = *((int*)&e2 + 1);

   simde__m128 ws1 = simde_mm_load_ss(&waveshapers[2][e11 & 0x3ff]);
   simde__m128 ws2 = simde_mm_load_ss(&waveshapers[2][e12 & 0x3ff]);
   simde__m128 ws3 = simde_mm_load_ss(&waveshapers[2][e21 & 0x3ff]);
   simde__m128 ws4 = simde_mm_load_ss(&waveshapers[2][e22 & 0x3ff]);
   simde__m128 ws = simde_mm_movelh_ps(simde_mm_unpacklo_ps(ws1, ws2), simde_mm_unpacklo_ps(ws3, ws4));
   ws1 = simde_mm_load_ss(&waveshapers[2][(e11 + 1) & 0x3ff]);
   ws2 = simde_mm_load_ss(&waveshapers[2][(e12 + 1) & 0x3ff]);
   ws3 = simde_mm_load_ss(&waveshapers[2][(e21 + 1) & 0x3ff]);
   ws4 = simde_mm_load_ss(&waveshapers[2][(e22 + 1) & 0x3ff]);
   simde__m128 wsn = simde_mm_movelh_ps(simde_mm_unpacklo_ps(ws1, ws2), simde_mm_unpacklo_ps(ws3, ws4));

   x = simde_mm_add_ps(simde_mm_mul_ps(simde_mm_sub_ps(one, a), ws), simde_mm_mul_ps(a, wsn));

   simde_mm_empty();

   return x;
}
#endif

simde__m128 ASYM_SSE2(simde__m128 in, simde__m128 drive)
{
   const simde__m128 one = simde_mm_set1_ps(1.f);
   const simde__m128 m32 = simde_mm_set1_ps(32.f);
   const simde__m128 m512 = simde_mm_set1_ps(512.f);
   const simde__m128i UB = simde_mm_set1_epi16(0x3fe);

   simde__m128 x = simde_mm_mul_ps(in, drive);
   x = simde_mm_add_ps(simde_mm_mul_ps(x, m32), m512);

   simde__m128i e = simde_mm_cvtps_epi32(x);
   simde__m128 a = simde_mm_sub_ps(x, simde_mm_cvtepi32_ps(e));
   e = simde_mm_packs_epi32(e, e);
   e = simde_mm_max_epi16(simde_mm_min_epi16(e, UB), simde_mm_setzero_si128());

#if MAC
   // this should be very fast on C2D/C1D (and there are no macs with K8's)
   int e4 alignas(16)[4];
   e4[0] = simde_mm_cvtsi128_si32(e);
   e4[1] = simde_mm_cvtsi128_si32(simde_mm_shufflelo_epi16(e, _MM_SHUFFLE(1, 1, 1, 1)));
   e4[2] = simde_mm_cvtsi128_si32(simde_mm_shufflelo_epi16(e, _MM_SHUFFLE(2, 2, 2, 2)));
   e4[3] = simde_mm_cvtsi128_si32(simde_mm_shufflelo_epi16(e, _MM_SHUFFLE(3, 3, 3, 3)));

#else
   // on PC write to memory & back as XMM -> GPR is slow on K8
   short e4 alignas(16)[8];
   simde_mm_store_si128((simde__m128i*)&e4, e);
#endif

   simde__m128 ws1 = simde_mm_load_ss(&waveshapers[2][e4[0] & 0x3ff]);
   simde__m128 ws2 = simde_mm_load_ss(&waveshapers[2][e4[1] & 0x3ff]);
   simde__m128 ws3 = simde_mm_load_ss(&waveshapers[2][e4[2] & 0x3ff]);
   simde__m128 ws4 = simde_mm_load_ss(&waveshapers[2][e4[3] & 0x3ff]);
   simde__m128 ws = simde_mm_movelh_ps(simde_mm_unpacklo_ps(ws1, ws2), simde_mm_unpacklo_ps(ws3, ws4));
   ws1 = simde_mm_load_ss(&waveshapers[2][(e4[0] + 1) & 0x3ff]);
   ws2 = simde_mm_load_ss(&waveshapers[2][(e4[1] + 1) & 0x3ff]);
   ws3 = simde_mm_load_ss(&waveshapers[2][(e4[2] + 1) & 0x3ff]);
   ws4 = simde_mm_load_ss(&waveshapers[2][(e4[3] + 1) & 0x3ff]);
   simde__m128 wsn = simde_mm_movelh_ps(simde_mm_unpacklo_ps(ws1, ws2), simde_mm_unpacklo_ps(ws3, ws4));

   x = simde_mm_add_ps(simde_mm_mul_ps(simde_mm_sub_ps(one, a), ws), simde_mm_mul_ps(a, wsn));

   return x;
}

FilterUnitQFPtr GetQFPtrFilterUnit(int type, int subtype)
{
   switch (type)
   {
   case fut_lp12:
   {
      if (subtype == st_SVF)
         return SVFLP12Aquad;
      else if (subtype == st_Rough)
         return IIR12CFCquad;
      //			else if(subtype==st_Medium)
      //				return IIR12CFLquad;
      return IIR12Bquad;
   }
   case fut_hp12:
   {
      if (subtype == st_SVF)
         return SVFHP12Aquad;
      else if (subtype == st_Rough)
         return IIR12CFCquad;
      //			else if(subtype==st_Medium)
      //				return IIR12CFLquad;
      return IIR12Bquad;
   }
   case fut_bp12:
   {
      if (subtype == st_SVF)
         return SVFBP12Aquad;
      else if (subtype == 3)
         return SVFBP24Aquad;
      else if (subtype == st_Rough)
         return IIR12CFCquad;
      //			else if(subtype==st_Medium)
      //				return SVFBP24Aquad;
      return IIR12Bquad;
   }
   case fut_br12:
      return IIR12Bquad;
   case fut_lp24:
      if (subtype == st_SVF)
         return SVFLP24Aquad;
      else if (subtype == st_Rough)
         return IIR24CFCquad;
      //		else if(subtype==st_Medium)
      //			return IIR24CFLquad;
      return IIR24Bquad;
   case fut_hp24:
      if (subtype == st_SVF)
         return SVFHP24Aquad;
      else if (subtype == st_Rough)
         return IIR24CFCquad;
      //		else if(subtype==st_Medium)
      //			return IIR24CFLquad;
      return IIR24Bquad;
   case fut_lpmoog:
      return LPMOOGquad;
   case fut_SNH:
      return SNHquad;
   case fut_comb:
      return COMBquad_SSE2;
   }
   return 0;
}

WaveshaperQFPtr GetQFPtrWaveshaper(int type)
{
   switch (type)
   {
   case wst_digi:
      return DIGI_SSE2;
   case wst_hard:
      return CLIP;
   case wst_sinus:
      return SINUS_SSE2;
   case wst_asym:
      return ASYM_SSE2;
   case wst_tanh:
      return TANH;
   }
   return 0;
}
