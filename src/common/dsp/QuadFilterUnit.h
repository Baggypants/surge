#include "FilterCoefficientMaker.h"
#pragma once
#include "globals.h"


const int n_filter_registers = 5;

struct QuadFilterUnitState
{
   simde__m128 C[n_cm_coeffs], dC[n_cm_coeffs]; // coefficients
   simde__m128 R[n_filter_registers];           // registers
   float* DB[4];                           // delay buffers
   int active[4]; // 0xffffffff if voice is active, 0 if not (usable as mask)
   int WP[4];     // comb write position
};

typedef simde__m128 (*FilterUnitQFPtr)(QuadFilterUnitState* __restrict, simde__m128 in);
typedef simde__m128 (*WaveshaperQFPtr)(simde__m128 in, simde__m128 drive);

FilterUnitQFPtr GetQFPtrFilterUnit(int type, int subtype);
WaveshaperQFPtr GetQFPtrWaveshaper(int type);
