#include "QuadFilterChain.h"
#include "SurgeStorage.h"
#include <vt_dsp/basic_dsp.h>
#include <vt_dsp/portable_intrinsics.h>

#define MWriteOutputs(x)                                                                           \
   d.OutL = simde_mm_add_ps(d.OutL, d.dOutL);                                                           \
   d.OutR = simde_mm_add_ps(d.OutR, d.dOutR);                                                           \
   simde__m128 outL = simde_mm_mul_ps(x, d.OutL);                                                            \
   simde__m128 outR = simde_mm_mul_ps(x, d.OutR);                                                            \
   simde_mm_store_ss(&OutL[k], simde_mm_add_ss(simde_mm_load_ss(&OutL[k]), sum_ps_to_ss(outL)));                  \
   simde_mm_store_ss(&OutR[k], simde_mm_add_ss(simde_mm_load_ss(&OutR[k]), sum_ps_to_ss(outR)));

#define MWriteOutputsDual(x, y)                                                                    \
   d.OutL = simde_mm_add_ps(d.OutL, d.dOutL);                                                           \
   d.OutR = simde_mm_add_ps(d.OutR, d.dOutR);                                                           \
   d.Out2L = simde_mm_add_ps(d.Out2L, d.dOut2L);                                                        \
   d.Out2R = simde_mm_add_ps(d.Out2R, d.dOut2R);                                                        \
   simde__m128 outL = vMAdd(x, d.OutL, vMul(y, d.Out2L));                                               \
   simde__m128 outR = vMAdd(x, d.OutR, vMul(y, d.Out2R));                                               \
   simde_mm_store_ss(&OutL[k], simde_mm_add_ss(simde_mm_load_ss(&OutL[k]), sum_ps_to_ss(outL)));                  \
   simde_mm_store_ss(&OutR[k], simde_mm_add_ss(simde_mm_load_ss(&OutR[k]), sum_ps_to_ss(outR)));

#if 0 // DEBUG
#define AssertReasonableAudioFloat(x) assert(x<32.f && x> - 32.f);
#else
#define AssertReasonableAudioFloat(x)
#endif

template <int config, bool A, bool WS, bool B>
void ProcessFBQuad(QuadFilterChainState& d, fbq_global& g, float* OutL, float* OutR)
{
   const simde__m128 hb_c = simde_mm_set1_ps(0.5f); // If this is changed from 0.5, make sure to change
                                          // this in the code because it is assumed to be half
   const simde__m128 one = simde_mm_set1_ps(1.0f);

   switch (config)
   {
   case fb_serial: // no feedback at all  (saves CPU)
      for (int k = 0; k < BLOCK_SIZE_OS; k++)
      {
         simde__m128 input = d.DL[k];
         simde__m128 x = input, y = d.DR[k];

         if (A)
            x = g.FU1ptr(&d.FU[0], x);
         if (WS)
         {
            d.wsLPF = simde_mm_mul_ps(hb_c, simde_mm_add_ps(d.wsLPF, x));
            d.Drive = simde_mm_add_ps(d.Drive, d.dDrive);
            x = g.WSptr(d.wsLPF, d.Drive);
         }

         if (A || WS)
         {
            d.Mix1 = simde_mm_add_ps(d.Mix1, d.dMix1);
            x = simde_mm_add_ps(simde_mm_mul_ps(input, simde_mm_sub_ps(one, d.Mix1)), simde_mm_mul_ps(x, d.Mix1));
         }

         y = simde_mm_add_ps(x, y);

         if (B)
            y = g.FU2ptr(&d.FU[1], y);

         d.Mix2 = simde_mm_add_ps(d.Mix2, d.dMix2);
         x = simde_mm_add_ps(simde_mm_mul_ps(x, simde_mm_sub_ps(one, d.Mix2)), simde_mm_mul_ps(y, d.Mix2));
         d.Gain = simde_mm_add_ps(d.Gain, d.dGain);
         simde__m128 mask = simde_mm_load_ps((float*)&d.FU[0].active);
         simde__m128 out = simde_mm_and_ps(mask, simde_mm_mul_ps(x, d.Gain));

         // output stage
         MWriteOutputs(out)
      }
      break;
   case fb_serial2:
      for (int k = 0; k < BLOCK_SIZE_OS; k++)
      {
         d.FB = simde_mm_add_ps(d.FB, d.dFB);
         simde__m128 input = vMul(d.FB, d.FBlineL);
         input = vAdd(d.DL[k], softclip_ps(input));
         simde__m128 x = input, y = d.DR[k];

         if (A)
            x = g.FU1ptr(&d.FU[0], x);
         if (WS)
         {
            d.wsLPF = simde_mm_mul_ps(hb_c, simde_mm_add_ps(d.wsLPF, x));
            d.Drive = simde_mm_add_ps(d.Drive, d.dDrive);
            x = g.WSptr(d.wsLPF, d.Drive);
         }

         if (A || WS)
         {
            d.Mix1 = simde_mm_add_ps(d.Mix1, d.dMix1);
            x = simde_mm_add_ps(simde_mm_mul_ps(input, simde_mm_sub_ps(one, d.Mix1)), simde_mm_mul_ps(x, d.Mix1));
         }

         y = simde_mm_add_ps(x, y);

         if (B)
            y = g.FU2ptr(&d.FU[1], y);

         d.Mix2 = simde_mm_add_ps(d.Mix2, d.dMix2);
         x = simde_mm_add_ps(simde_mm_mul_ps(x, simde_mm_sub_ps(one, d.Mix2)), simde_mm_mul_ps(y, d.Mix2));
         d.Gain = simde_mm_add_ps(d.Gain, d.dGain);
         simde__m128 mask = simde_mm_load_ps((float*)&d.FU[0].active);
         simde__m128 out = simde_mm_and_ps(mask, simde_mm_mul_ps(x, d.Gain));
         d.FBlineL = out;

         // output stage
         MWriteOutputs(out)
      }
      break;
   case fb_serial3: // filter 2 is only heard in the feedback path, good for physical modelling with
                    // comb as f2
      for (int k = 0; k < BLOCK_SIZE_OS; k++)
      {
         d.FB = simde_mm_add_ps(d.FB, d.dFB);
         simde__m128 input = vMul(d.FB, d.FBlineL);
         input = vAdd(d.DL[k], softclip_ps(input));
         simde__m128 x = input, y = d.DR[k];

         if (A)
            x = g.FU1ptr(&d.FU[0], x);
         if (WS)
         {
            d.wsLPF = simde_mm_mul_ps(hb_c, simde_mm_add_ps(d.wsLPF, x));
            d.Drive = simde_mm_add_ps(d.Drive, d.dDrive);
            x = g.WSptr(d.wsLPF, d.Drive);
         }

         if (A || WS)
         {
            d.Mix1 = simde_mm_add_ps(d.Mix1, d.dMix1);
            x = simde_mm_add_ps(simde_mm_mul_ps(input, simde_mm_sub_ps(one, d.Mix1)), simde_mm_mul_ps(x, d.Mix1));
         }

         // output stage
         d.Gain = simde_mm_add_ps(d.Gain, d.dGain);
         simde__m128 mask = simde_mm_load_ps((float*)&d.FU[0].active);
         x = simde_mm_and_ps(mask, simde_mm_mul_ps(x, d.Gain));

         MWriteOutputs(x)

             y = simde_mm_add_ps(x, y);

         if (B)
            y = g.FU2ptr(&d.FU[1], y);

         d.Mix2 = simde_mm_add_ps(d.Mix2, d.dMix2);
         x = simde_mm_add_ps(simde_mm_mul_ps(x, simde_mm_sub_ps(one, d.Mix2)), simde_mm_mul_ps(y, d.Mix2));

         d.FBlineL = y;
      }
      break;
   case fb_dual:
      for (int k = 0; k < BLOCK_SIZE_OS; k++)
      {
         d.FB = simde_mm_add_ps(d.FB, d.dFB);
         simde__m128 fb = simde_mm_mul_ps(d.FB, d.FBlineL);
         fb = softclip_ps(fb);
         simde__m128 x = simde_mm_add_ps(d.DL[k], fb);
         simde__m128 y = simde_mm_add_ps(d.DR[k], fb);

         if (A)
            x = g.FU1ptr(&d.FU[0], x);
         if (B)
            y = g.FU2ptr(&d.FU[1], y);

         d.Mix1 = simde_mm_add_ps(d.Mix1, d.dMix1);
         d.Mix2 = simde_mm_add_ps(d.Mix2, d.dMix2);
         x = simde_mm_add_ps(simde_mm_mul_ps(x, d.Mix1), simde_mm_mul_ps(y, d.Mix2));

         if (WS)
         {
            d.wsLPF = simde_mm_mul_ps(hb_c, simde_mm_add_ps(d.wsLPF, x));
            d.Drive = simde_mm_add_ps(d.Drive, d.dDrive);
            x = g.WSptr(d.wsLPF, d.Drive);
         }

         d.Gain = simde_mm_add_ps(d.Gain, d.dGain);
         simde__m128 mask = simde_mm_load_ps((float*)&d.FU[0].active);
         simde__m128 out = simde_mm_and_ps(mask, simde_mm_mul_ps(x, d.Gain));
         d.FBlineL = out;
         // output stage
         MWriteOutputs(out)
      }
      break;
   case fb_dual2:
      for (int k = 0; k < BLOCK_SIZE_OS; k++)
      {
         d.FB = simde_mm_add_ps(d.FB, d.dFB);
         simde__m128 fb = simde_mm_mul_ps(d.FB, d.FBlineL);
         fb = softclip_ps(fb);
         simde__m128 x = simde_mm_add_ps(d.DL[k], fb);
         simde__m128 y = simde_mm_add_ps(d.DR[k], fb);

         if (A)
            x = g.FU1ptr(&d.FU[0], x);
         if (WS)
         {
            d.wsLPF = simde_mm_mul_ps(hb_c, simde_mm_add_ps(d.wsLPF, x));
            d.Drive = simde_mm_add_ps(d.Drive, d.dDrive);
            x = g.WSptr(d.wsLPF, d.Drive);
         }

         if (B)
            y = g.FU2ptr(&d.FU[1], y);

         d.Mix1 = simde_mm_add_ps(d.Mix1, d.dMix1);
         d.Mix2 = simde_mm_add_ps(d.Mix2, d.dMix2);
         x = simde_mm_add_ps(simde_mm_mul_ps(x, d.Mix1), simde_mm_mul_ps(y, d.Mix2));

         d.Gain = simde_mm_add_ps(d.Gain, d.dGain);
         simde__m128 mask = simde_mm_load_ps((float*)&d.FU[0].active);
         simde__m128 out = simde_mm_and_ps(mask, simde_mm_mul_ps(x, d.Gain));
         d.FBlineL = out;
         // output stage
         MWriteOutputs(out)
      }
      break;
   case fb_ring:
      for (int k = 0; k < BLOCK_SIZE_OS; k++)
      {
         d.FB = simde_mm_add_ps(d.FB, d.dFB);
         simde__m128 fb = simde_mm_mul_ps(d.FB, d.FBlineL);
         fb = softclip_ps(fb);
         simde__m128 x = simde_mm_add_ps(d.DL[k], fb);
         simde__m128 y = simde_mm_add_ps(d.DR[k], fb);

         if (A)
            x = g.FU1ptr(&d.FU[0], x);
         if (B)
            y = g.FU2ptr(&d.FU[1], y);

         d.Mix1 = simde_mm_add_ps(d.Mix1, d.dMix1);
         d.Mix2 = simde_mm_add_ps(d.Mix2, d.dMix2);

         x = simde_mm_mul_ps(simde_mm_add_ps(simde_mm_mul_ps(simde_mm_sub_ps(one, d.Mix1), y), simde_mm_mul_ps(x, d.Mix1)),
                        simde_mm_add_ps(simde_mm_mul_ps(simde_mm_sub_ps(one, d.Mix2), x), simde_mm_mul_ps(y, d.Mix2)));

         if (WS)
         {
            d.wsLPF = simde_mm_mul_ps(hb_c, simde_mm_add_ps(d.wsLPF, x));
            d.Drive = simde_mm_add_ps(d.Drive, d.dDrive);
            x = g.WSptr(d.wsLPF, d.Drive);
         }

         d.Gain = simde_mm_add_ps(d.Gain, d.dGain);
         simde__m128 mask = simde_mm_load_ps((float*)&d.FU[0].active);
         simde__m128 out = simde_mm_and_ps(mask, simde_mm_mul_ps(x, d.Gain));
         d.FBlineL = out;
         // output stage
         MWriteOutputs(out)
      }
      break;
   case fb_stereo:
      for (int k = 0; k < BLOCK_SIZE_OS; k++)
      {
         d.FB = simde_mm_add_ps(d.FB, d.dFB);
         simde__m128 fb = simde_mm_mul_ps(d.FB, d.FBlineL);
         fb = softclip_ps(fb);
         simde__m128 x = simde_mm_add_ps(d.DL[k], fb);
         simde__m128 y = simde_mm_add_ps(d.DR[k], fb);

         if (A)
            x = g.FU1ptr(&d.FU[0], x);
         if (B)
            y = g.FU2ptr(&d.FU[1], y);

         if (WS)
         {
            d.Drive = simde_mm_add_ps(d.Drive, d.dDrive);
            x = g.WSptr(x, d.Drive);
            y = g.WSptr(y, d.Drive);
         }

         d.Mix1 = simde_mm_add_ps(d.Mix1, d.dMix1);
         d.Mix2 = simde_mm_add_ps(d.Mix2, d.dMix2);
         x = simde_mm_mul_ps(x, d.Mix1);
         y = simde_mm_mul_ps(y, d.Mix2);

         d.Gain = simde_mm_add_ps(d.Gain, d.dGain);
         simde__m128 mask = simde_mm_load_ps((float*)&d.FU[0].active);
         x = simde_mm_and_ps(mask, simde_mm_mul_ps(x, d.Gain));
         y = simde_mm_and_ps(mask, simde_mm_mul_ps(y, d.Gain));
         d.FBlineL = simde_mm_add_ps(x, y);

         // output stage
         MWriteOutputsDual(x, y) AssertReasonableAudioFloat(OutL[k]);
         AssertReasonableAudioFloat(OutR[k]);
      }
      break;
   case fb_wide:
      for (int k = 0; k < BLOCK_SIZE_OS; k++)
      {
         d.FB = simde_mm_add_ps(d.FB, d.dFB);
         simde__m128 fbL = simde_mm_mul_ps(d.FB, d.FBlineL);
         simde__m128 fbR = simde_mm_mul_ps(d.FB, d.FBlineR);
         simde__m128 xin = simde_mm_add_ps(d.DL[k], softclip_ps(fbL));
         simde__m128 yin = simde_mm_add_ps(d.DR[k], softclip_ps(fbR));
         simde__m128 x = xin;
         simde__m128 y = yin;

         if (A)
         {
            x = g.FU1ptr(&d.FU[0], x);
            y = g.FU1ptr(&d.FU[2], y);
         }

         if (WS)
         {
            d.Drive = simde_mm_add_ps(d.Drive, d.dDrive);
            x = g.WSptr(x, d.Drive);
            y = g.WSptr(y, d.Drive);
         }

         if (A || WS)
         {
            d.Mix1 = simde_mm_add_ps(d.Mix1, d.dMix1);
            simde__m128 t = simde_mm_sub_ps(one, d.Mix1);
            x = simde_mm_add_ps(simde_mm_mul_ps(xin, t), simde_mm_mul_ps(x, d.Mix1));
            y = simde_mm_add_ps(simde_mm_mul_ps(yin, t), simde_mm_mul_ps(y, d.Mix1));
         }

         if (B)
         {
            simde__m128 z = g.FU2ptr(&d.FU[1], x);
            simde__m128 w = g.FU2ptr(&d.FU[3], y);

            d.Mix2 = simde_mm_add_ps(d.Mix2, d.dMix2);
            simde__m128 t = simde_mm_sub_ps(one, d.Mix2);
            x = simde_mm_add_ps(simde_mm_mul_ps(x, t), simde_mm_mul_ps(z, d.Mix2));
            y = simde_mm_add_ps(simde_mm_mul_ps(y, t), simde_mm_mul_ps(w, d.Mix2));
         }

         d.Gain = simde_mm_add_ps(d.Gain, d.dGain);
         simde__m128 mask = simde_mm_load_ps((float*)&d.FU[0].active);
         x = simde_mm_and_ps(mask, simde_mm_mul_ps(x, d.Gain));
         y = simde_mm_and_ps(mask, simde_mm_mul_ps(y, d.Gain));
         d.FBlineL = x;
         d.FBlineR = y;

         // output stage
         MWriteOutputsDual(x, y) AssertReasonableAudioFloat(OutL[k]);
         AssertReasonableAudioFloat(OutR[k]);
      }
      break;
   }
}

template <int config> FBQFPtr GetFBQPointer2(bool A, bool WS, bool B)
{
   if (A)
   {
      if (B)
      {
         if (WS)
            return ProcessFBQuad<config, 1, 1, 1>;
         else
            return ProcessFBQuad<config, 1, 0, 1>;
      }
      else
      {
         if (WS)
            return ProcessFBQuad<config, 1, 1, 0>;
         else
            return ProcessFBQuad<config, 1, 0, 0>;
      }
   }
   else
   {
      if (B)
      {
         if (WS)
            return ProcessFBQuad<config, 0, 1, 1>;
         else
            return ProcessFBQuad<config, 0, 0, 1>;
      }
      else
      {
         if (WS)
            return ProcessFBQuad<config, 0, 1, 0>;
         else
            return ProcessFBQuad<config, 0, 0, 0>;
      }
   }
   return 0;
}

FBQFPtr GetFBQPointer(int config, bool A, bool WS, bool B)
{
   switch (config)
   {
   case fb_serial:
      return GetFBQPointer2<fb_serial>(A, WS, B);
   case fb_serial2:
      return GetFBQPointer2<fb_serial2>(A, WS, B);
   case fb_serial3:
      return GetFBQPointer2<fb_serial3>(A, WS, B);
   case fb_dual:
      return GetFBQPointer2<fb_dual>(A, WS, B);
   case fb_dual2:
      return GetFBQPointer2<fb_dual2>(A, WS, B);
   case fb_ring:
      return GetFBQPointer2<fb_ring>(A, WS, B);
   case fb_stereo:
      return GetFBQPointer2<fb_stereo>(A, WS, B);
   case fb_wide:
      return GetFBQPointer2<fb_wide>(A, WS, B);
   }
   return 0;
}

void InitQuadFilterChainStateToZero(QuadFilterChainState *Q)
{
    Q->Gain = simde_mm_setzero_ps();
    Q->FB = simde_mm_setzero_ps();
    Q->Mix1 = simde_mm_setzero_ps();
    Q->Mix2 = simde_mm_setzero_ps();
    Q->Drive = simde_mm_setzero_ps();
    Q->dGain = simde_mm_setzero_ps();
    Q->dFB = simde_mm_setzero_ps();
    Q->dMix1 = simde_mm_setzero_ps();
    Q->dMix2 = simde_mm_setzero_ps();
    Q->dDrive = simde_mm_setzero_ps();
    
    Q->wsLPF = simde_mm_setzero_ps();
    Q->FBlineL = simde_mm_setzero_ps();
    Q->FBlineR = simde_mm_setzero_ps();
    
    for(auto i=0; i<BLOCK_SIZE_OS; ++i)
    {
        Q->DL[i] = simde_mm_setzero_ps();
        Q->DR[i] = simde_mm_setzero_ps();
    }
    
    Q->OutL = simde_mm_setzero_ps();
    Q->OutR = simde_mm_setzero_ps();
    Q->dOutL = simde_mm_setzero_ps();
    Q->dOutR = simde_mm_setzero_ps();
    Q->Out2L = simde_mm_setzero_ps();
    Q->Out2R = simde_mm_setzero_ps();
    Q->dOut2L = simde_mm_setzero_ps();
    Q->dOut2R = simde_mm_setzero_ps();
}
